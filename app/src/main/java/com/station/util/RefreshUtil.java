package com.station.util;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.view.LayoutInflater;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;

import com.station.R;
import com.station.databinding.ViewPtrrefresherBinding;
import com.station.listener.RefreshListener;

import in.srain.cube.views.ptr.PtrClassicFrameLayout;
import in.srain.cube.views.ptr.PtrDefaultHandler2;
import in.srain.cube.views.ptr.PtrFrameLayout;
import in.srain.cube.views.ptr.PtrUIHandler;
import in.srain.cube.views.ptr.indicator.PtrIndicator;

/**
 * Created by nbzl on 2017/8/15.
 */
public class RefreshUtil {
    public PtrClassicFrameLayout refreshlayout;
    public RefreshListener listener;
    public int mState;
    public Context context;
    public Animation rotate;
    public ViewPtrrefresherBinding headerBinding,footerBinding;
    public RefreshUtil(Context context, PtrClassicFrameLayout refreshlayout, RefreshListener listener){
        this.context=context;
        this.refreshlayout=refreshlayout;
        this.listener=listener;
        this.headerBinding= DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.view_ptrrefresher, null, false);
        this.footerBinding= DataBindingUtil.inflate(LayoutInflater.from(context),R.layout.view_ptrrefresher, null, false);
        this.refreshlayout.setHeaderView(headerBinding.refresh);
        this.refreshlayout.setFooterView(footerBinding.refresh);
    }

    public void initRefreshView(){
        refreshlayout.setPtrHandler(new PtrDefaultHandler2() {
            @Override
            public void onLoadMoreBegin(PtrFrameLayout frame) { // 上拉加载的回调方法
                //refreshlayout.refreshComplete();
                listener.loadmore();
                /*if(isMore){
                    page++;
                    requestData();
                }else{
                    binding.ptrLayout.refreshComplete();
                    binding.recycleview.smoothScrollToPosition(datalist.size() - 1);
                }*/
            }

            @Override
            public void onRefreshBegin(PtrFrameLayout frame) { // 下拉刷新的回调方法
                //refreshlayout.refreshComplete();
                listener.refresh();
                /*page=1;
                content="";
                requestData();*/
            }
        });

        refreshlayout.addPtrUIHandler(new PtrUIHandler() {
            @Override
            public void onUIReset(PtrFrameLayout frame) {
                mState = Static.STATE_RESET;
            }

            @Override
            public void onUIRefreshPrepare(PtrFrameLayout frame) {
                mState = Static.STATE_PREPARE;
            }

            @Override
            public void onUIRefreshBegin(PtrFrameLayout frame) {
                mState = Static.STATE_BEGIN;
                rotate= AnimationUtils.loadAnimation(context,R.anim.refresh);
                LinearInterpolator lin = new LinearInterpolator();
                rotate.setInterpolator(lin);
                headerBinding.statusImg.startAnimation(rotate);

                footerBinding.statusImg.startAnimation(rotate);
            }

            @Override
            public void onUIRefreshComplete(PtrFrameLayout frame, boolean isHeader) {
                mState = Static.STATE_FINISH;
                headerBinding.statusImg.clearAnimation();
                footerBinding.statusImg.clearAnimation();
            }

            @Override
            public void onUIPositionChange(PtrFrameLayout frame, boolean isUnderTouch, byte status, PtrIndicator ptrIndicator) {
                switch (mState) {
                    case Static.STATE_PREPARE:
                        headerBinding.statusImg.setRotation(ptrIndicator.getCurrentPercent()*180);
                        footerBinding.statusImg.setRotation(ptrIndicator.getCurrentPercent()*180);
                        if (ptrIndicator.getCurrentPercent()<1.5){
                            headerBinding.statusTxt.setText("下拉刷新...");
                            footerBinding.statusTxt.setText("下拉刷新...");
                        }else{
                            headerBinding.statusTxt.setText("放开以刷新...");
                            footerBinding.statusTxt.setText("放开以刷新...");
                        }
                        break;
                    case Static.STATE_BEGIN:
                        headerBinding.statusTxt.setText("正在加载中...");
                        footerBinding.statusTxt.setText("正在加载中...");
                        break;
                    case Static.STATE_FINISH:
                        headerBinding.statusTxt.setText("加载成功...");
                        footerBinding.statusTxt.setText("加载成功...");
                        break;
                }

            }
        });

    }

}
