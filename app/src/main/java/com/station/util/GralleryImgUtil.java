package com.station.util;

import android.content.Context;
import android.util.Log;

import com.station.listener.GralleryImgListener;

import java.io.File;
import java.util.Calendar;

import cn.finalteam.rxgalleryfinal.RxGalleryFinal;
import cn.finalteam.rxgalleryfinal.imageloader.ImageLoaderType;
import cn.finalteam.rxgalleryfinal.rxbus.RxBusResultSubscriber;
import cn.finalteam.rxgalleryfinal.rxbus.event.ImageRadioResultEvent;
import cn.finalteam.rxgalleryfinal.utils.BitmapUtils;

/**
 * Created by nbzl on 2017/8/17.
 */
public class GralleryImgUtil {
    public static GralleryImgUtil util;
    private RxGalleryFinal rx;
    private Context context;
    private GralleryImgListener listener;

    public static GralleryImgUtil getInstance(Context context,GralleryImgListener listener) {
        if (util == null) {
            util = new GralleryImgUtil();
        }
        util.context = context;
        util.listener=listener;
        return util;
    }

    public void showImgList() {
        rx = RxGalleryFinal
                .with(context)
                .image()
                .radio()
                .imageLoader(ImageLoaderType.GLIDE)
                .subscribe(new RxBusResultSubscriber<ImageRadioResultEvent>() {
                    @Override
                    protected void onEvent(ImageRadioResultEvent imageRadioResultEvent) throws Exception {
                        try {
                            String name=(Calendar.getInstance().getTime().getTime())+imageRadioResultEvent.getResult().getOriginalPath().substring(imageRadioResultEvent.getResult().getOriginalPath().lastIndexOf("."),imageRadioResultEvent.getResult().getOriginalPath().length());
                            Log.e("Mytext","name--->"+name);
                            Log.e("Mytext","path--->"+imageRadioResultEvent.getResult().getOriginalPath());
                            Log.e("Mytext","cache--->"+FileUtil.getInstance().imgCache);
                            Log.e("Mytext","root--->"+imageRadioResultEvent.getResult().getOriginalPath().substring(0,imageRadioResultEvent.getResult().getOriginalPath().lastIndexOf("/")));
                            BitmapUtils.compressAndSaveImage(FileUtil.getInstance().createImg(name), imageRadioResultEvent.getResult().getOriginalPath(), 1);
                            listener.success(FileUtil.getInstance().imgCache+ File.separator+name);
                        } catch (Exception e) {
                            Log.e("Mytext", "bug-->" + e.getMessage());
                            listener.fail("照片获取失败");
                        }
                    }
                });
        rx.openGallery();
    }
}
