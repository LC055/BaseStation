package com.station.util;

import android.content.Context;
import android.widget.Toast;

/**
 * Created by nbzl on 2017/10/9.
 */
public class Static {
    public static final int STATE_RESET = -1;
    public static final int STATE_PREPARE = 0;
    public static final int STATE_BEGIN = 1;
    public static final int STATE_FINISH = 2;

    public static String policecode="000006";

    public static String rows="4";
    public static String zl="zl";
    public static String basestation="basestation";

    public static String NET_ERROR="网络连接失败";//无网络
    public static String DATA_ERROR="数据有误";
    public static String DATA_EMPTY="暂无更多数据";
    public static String TIMEEND="已到时";

    public static String APPSERVER="http://20.65.2.12:4004/jzxj";
    //public static String APPSERVER="http://192.168.199.103:8080/zlapp";

    public static String XJLIST=APPSERVER+"/api/android/nochecklist";//未巡检
    public static String RALIST=APPSERVER+"/api/android/radiolist";//电台列表
    public static String BSList=APPSERVER+"/api/android/basestationlist";//基站列表
    public static String XJDATALIST=APPSERVER+"/api/android/patrolinputlist";//巡检列表
    public static String BASELIST=APPSERVER+"/api/android/getLinkBasicNameJson";//基站列表
    public static String LINKLIST=APPSERVER+"/api/android/getNameJson";//运营商列表
    public static String CJDWLIST=APPSERVER+"/api/android/getContractorNameJson";//承建单位
    public static String CheckList=APPSERVER+"/api/android/getCheckSortList1";//

    public static String TAB1DETAIL=APPSERVER+"/api/android/basestationShowInfo";//基站详情
    public static String TAB2DETAIL=APPSERVER+"/api/android/patrolinputShowInfo";//巡检基本信息详情 图片详情
    public static String TAB22DETAIL=APPSERVER+"/api/android/getCheckSortList";//巡检检查项
    public static String PPLIST=APPSERVER+"/api/android/getBrandNameJson";//品牌列表

    public static String DELTAB1=APPSERVER+"/api/android/deleteBasestation";//基站删除
    public static String DELTAB2=APPSERVER+"/api/android/deletePatrolinput";//巡检删除
    public static String DELTAB4=APPSERVER+"/api/android/deleteRadio";//电台删除



    public static String INPUTTAB1=APPSERVER+"/api/android/insertBasestation";//基站录入
    public static String INPUTTAB2=APPSERVER+"/api/android/insertPatrolinput";//fjids 巡检录入
    public static String INPUTTAB4=APPSERVER+"/api/android/insertRadio";//电台录入
    //public static String TAB23DETAIL=APPSERVER+"/api/android/patrolinputShowInfo";//巡检


    public static String UPIMG=APPSERVER+"/api/android/upload";//上传照片
    public static String GETIMG=APPSERVER+"/api/android/getimg?pic_path=";

    public static void showToast(Context context, String msg){
        Toast.makeText(context,msg,Toast.LENGTH_SHORT).show();
    }
}
