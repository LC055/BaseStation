package com.station.util;

import com.google.gson.Gson;

import java.util.Arrays;
import java.util.List;

/**
 * Created by nbzl on 2017/8/2.
 */
public class GsonUtil {
    public static GsonUtil gsonUtil;
    public static Gson gson;

    public static GsonUtil getInstance(){
        if(gsonUtil==null){
            gsonUtil=new GsonUtil();
            gsonUtil.gson=new Gson();
        }
        return gsonUtil;
    }

    public static <T> T getObject(String jsonString,Class<T> cls){
        T t = null;
        try{
            t = gson.fromJson(jsonString,cls);
        }catch(Exception e) {
            e.printStackTrace();
        }
        return t;
    }

    public static <T> List<T> getArray(String jsonString, Class<T[]> cls){
        Gson gson = new Gson();
        T[] array = gson.fromJson(jsonString, cls);
        return Arrays.asList(array);
    }

    public static String getJson(Object object){
        return gson.toJson(object);
    }
}
