package com.station.util;

import android.widget.EditText;
import android.widget.TextView;

/**
 * Created by nbzl on 2017/8/8.
 */
public class BindingUtil {

    public static String isEmpty(String label,String content){
        if(content==null||content.equals("")){
            return label+"/";
        }else {
            return label+content;
        }
    }

    public static String isEmpty(String label,String content,String replace){
        if(content==null||content.equals("")){
            return label+replace;
        }else {
            return label+content;
        }
    }

    public static String isEmpty(String label1,String content1,String label2,String content2){
        String result="";
        if(content1==null||content1.equals("")){
            result+=label1+"/";
        }else {
            result+=label1+content1;
        }
        result+="  ";
        if(content2==null||content2.equals("")){
            result+=label2+"/";
        }else {
            result+=label2+content2;
        }

        return result;
    }

    public static String isEmpty(String content){
        if(content==null||content.equals("")){
            return "";
        }else {
            return content;
        }
    }

    public static String isEmpty(EditText editText){
        return isEmpty(editText.getText().toString());
    }
    public static String isEmpty(TextView editText){
        return isEmpty(editText.getText().toString());
    }


    public static String isCheck(String label,String content){
        if(content==null||content.equals("")){
            return label+"/";
        }else {
            if(Integer.valueOf(content)==0){
                return label+"否";
            }else if (Integer.valueOf(content)==1){
                return label+"是";
            }
        }
        return "";
    }

    public static String isRate(String label,String content){
        if(content==null||content.equals("")){
            return label+"/";
        }else {
            if(Integer.valueOf(content)==0){
                return label+"一个月";
            }else if(Integer.valueOf(content)==1){
                return label+"一个季度";
            }
        }
        return label+"/";
    }

    public static String isXJ(String label,String content){
        if(content==null||content.equals("")){
            return label+"未巡检";
        }else {
            if(Integer.valueOf(content)==0){
                return label+"未巡检";
            }else if (Integer.valueOf(content)==1){
                return label+"已巡检";
            }
        }
        return label+"未巡检";
    }

    public static String isDtType(String label,String content){//012
        if(content==null||content.equals("")){
            return label+"/";
        }else {
            if(Integer.valueOf(content)==0){
                return label+"备用";
            }else if (Integer.valueOf(content)==1){
                return label+"在用";
            }else if (Integer.valueOf(content)==2){
                return label+"注销";
            }else if (Integer.valueOf(content)==3){
            return label+"外接";
        }
        }
        return label+"/";
    }

    public static boolean isNull(Object obj){
        if (obj==null){
            return false;
        }else {
            return true;
        }
    }

    public static String isNormal(String label,String content){
        if(content==null||content.equals("")){
            return label+"/";
        }else {
            if(Integer.valueOf(content)==0){
                return label+"不正常";
            }else if (Integer.valueOf(content)==1){
                return label+"正常";
            }
        }
        return label+"/";
    }

    public static String isSolve(String label,String content){
        if(content==null||content.equals("")){
            return label+"/";
        }else {
            if(Integer.valueOf(content)==0){
                return label+"未解决";
            }else if (Integer.valueOf(content)==1){
                return label+"已解决";
            }
        }
        return label+"/";
    }

}
