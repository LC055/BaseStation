package com.station.util;

import android.app.Activity;
import android.databinding.DataBindingUtil;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;

import com.station.R;
import com.station.databinding.LayoutLoadBinding;
import com.station.listener.ViewClickListener;


/**
 * Created by nbzl on 2017/8/10.
 */
public class LoadLayoutModel {
    public Activity context;
    public LayoutLoadBinding binding;
    public ViewClickListener listener;
    public static String EMPTY="数据为空";

    public LoadLayoutModel(Activity context, ViewClickListener listener){
        this.context=context;
        this.listener=listener;
        binding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.layout_load,null,false);
        setLoadStatus(true,EMPTY,0);
    }

    public void setLoadStatus(boolean flag,String statusMsg,int drawable){
        binding.setClick(this);
        if(flag){
            binding.loadView.setVisibility(View.VISIBLE);
        }else{
            binding.loadView.setVisibility(View.GONE);
        }

        binding.setIsLoad(flag);
        binding.setStatusTxt(statusMsg);
        if(drawable==0){
            binding.setDrawable(ContextCompat.getDrawable(context,R.drawable.net_fail));
        }else{
            binding.setDrawable(context.getResources().getDrawable(drawable));
        }

    }

    public LayoutLoadBinding getBinding(){
        return binding;
    }

    public void OnItemClick(View view){
        listener.reRequest(view);
    }
}
