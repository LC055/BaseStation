package com.station.util;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by nbzl on 2017/8/11.
 */
public class PreferencesUtil {
    private final String IS_DOWN="isdownread";
    private final String READ_PACKAGENAME= "readPackageName";
    private final String READ_PACKAGEDESC= "readPackageDesc";
    private static String PREF_POLICECODE = "policecode";
    private static String ISREFRESH="refresh";
    private static final String LATITUDE="latitude",LONGITUDE="longitude";
    private static String SFZH_POLICECODE = "sfzh";
    //使用SharedPreferences来存储用户设置
    private SharedPreferences mShared = null;
    //context
    private Context mContext = null;
    private static PreferencesUtil mInstance = null;

    public static PreferencesUtil getInstance(Context context){
        if(null == mInstance && null != context){
            mInstance = new PreferencesUtil(context.getApplicationContext());
        }
        return mInstance;
    }
    private PreferencesUtil(Context context){
        mContext = context;
        mShared = mContext.getSharedPreferences("com.fkapp", Context.MODE_PRIVATE);
    }


    public String getLastLoginPoliceCode() {
        return mShared.getString(PREF_POLICECODE, "000000");
    }
    public void setLastLoginPoliceCode(String policeCode) {
        SharedPreferences.Editor edit = mShared.edit();
        edit.putString(PREF_POLICECODE,policeCode);
        edit.commit();
    }

    //是否下载安装
    public boolean setIsDownRead(boolean isDown){
        SharedPreferences.Editor edit = mShared.edit();
        edit.putBoolean(IS_DOWN, isDown);
        return edit.commit();
    }
    public boolean getIsDownRead(){
        return mShared.getBoolean(IS_DOWN, false);
    }
    public boolean setReadPackageName(String readPackageName){
        SharedPreferences.Editor edit = mShared.edit();
        if(null != readPackageName){
            edit.putString(READ_PACKAGENAME, readPackageName);
        }
        return edit.commit();
    }
    public String getReadPackageName(){
        return mShared.getString(READ_PACKAGENAME, "");
    }
    public boolean setReadPackageDesc(String readPackageDesc){
        SharedPreferences.Editor edit = mShared.edit();
        if(null != readPackageDesc){
            edit.putString(READ_PACKAGEDESC, readPackageDesc);
//			edit.putLong(LAST_LOGIN_TIME, time);
        }
        return edit.commit();
    }

    public String getLongitude(){
        return mShared.getString(LONGITUDE,"");
    }
    public void setLongitude(String longitude){
        SharedPreferences.Editor editor = mShared.edit();
        editor.putString(LONGITUDE,longitude);
        editor.commit();
    }

    public String getLatitude(){
        return mShared.getString(LATITUDE,"");
    }
    public void setLatitude(String latitude){
        SharedPreferences.Editor editor = mShared.edit();
        editor.putString(LATITUDE,latitude);
        editor.commit();
    }

    public String getReadPackageDesc(){
        return mShared.getString(READ_PACKAGEDESC, "下载应用模块");
    }

    public String getLoginSFZH(){
        return mShared.getString(SFZH_POLICECODE,"");
    }
    public void setLoginSFZH(String sfzh) {
        SharedPreferences.Editor edit = mShared.edit();
        edit.putString(SFZH_POLICECODE,sfzh);
        edit.commit();
    }

}
