package com.station.xml;

import java.util.ArrayList;
import java.util.List;


/**
 * 业务数据封装
 * @author hehc
 *
 */
public class Data {
	
	public static String TAG_DETAIL = "detail";
	
	private final Class<?> clazz;
	private List<Object> details = new ArrayList<Object>(0);
	
	public Data(Class<?> clazz){
		this.clazz = clazz;
	}
	
	public List<Object> getDetails() {
		return details;
	}

	public void setDetails(List<Object> details) {
		this.details = details;
	}
	
	/**
	 * 当交互数据返回的是表单数据时，直接调用此即可
	 * @return 自定义业务Model对象
	 */
	public Object getFormData(){
		if(!details.isEmpty() && details.size() == 1){
			return details.get(0);
		}else{
			throw new RuntimeException("非表单数据，错误的调用");
		}
	}
	
	public Object newDetail(){
		Object a = null;
		try {
			a = clazz.newInstance();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		return a;
	}
	
}
