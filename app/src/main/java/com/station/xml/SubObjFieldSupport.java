package com.station.xml;

import java.util.ArrayList;
import java.util.List;

/**
 * 如果model有对象属性，对象属性也需要实现自动解析，由此类派生，只支持一级子对象
 * @author hehc
 *
 */
public abstract class SubObjFieldSupport extends KVSupport{
	
	/**
	 * 存放所有的子对象结果，需要重写方法自行实现注入
	 */
	private List<Object> childs;

	public List<Object> getChilds() {
		if(childs == null)
			childs = new ArrayList<Object>(0);
		return childs;
	}

	public void setChilds(List<Object> childs) {
		this.childs = childs;
	}
	
	/**
	 * 需实现将子对象注入到对应的真实Field中
	 */
	public abstract void childsToFields();

}
