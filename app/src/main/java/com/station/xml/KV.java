package com.station.xml;

public class KV {
	String key;
	Object value;
	
	public KV(String key, Object value) {
		super();
		this.key = key;
		this.value = value;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public Object getValue() {
		return value==null?"":value;
	}
	public String getStringValue(){
		return getValue().toString();
	}
	public void setValue(Object value) {
		this.value = value;
	}
}
