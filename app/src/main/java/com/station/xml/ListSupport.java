package com.station.xml;
/**
 * 如果model需要展示列表，由此类派生
 * @author hehc
 *
 */
public class ListSupport{

	private ListItem listItem;

	public ListItem getListItem() {
		if(listItem == null)
			listItem = new ListItem();
		return listItem;
	}

	public void setListItem(ListItem listItem) {
		this.listItem = listItem;
	}

	/**
	 * 列表行数据封装
	 * 
	 * @author hehc
	 * 
	 */
	public static class ListItem {

		public static final String ANNOTATION_ID = "id";
		public static final String ANNOTATION_TITLE = "title";
		public static final String ANNOTATION_SUMMARY = "summary";
		public static final String ANNOTATION_TAG = "tag";

		private String id;// data entity id
		private String title;// 标题
		private String summary;// 附属内容（下左）
		private String tag;// 标记内容（下右）
		private Object extra;// 附加信息

		public ListItem() {
			super();
		}

		public ListItem(String id, String title) {
			super();
			this.id = id;
			this.title = title;
		}

		public String getId() {
			return id==null?"":id;
		}

		public void setId(String id) {
			this.id = id;
		}

		public String getTitle() {
			return title==null?"无":(title.trim().equals("")?"无":title);
		}

		public void setTitle(String title) {
			this.title = title;
		}

		public String getSummary() {
			return summary==null?"":summary;
		}

		public void setSummary(String summary) {
			this.summary = summary;
		}

		public String getTag() {
			return tag==null?"":tag;
		}

		public void setTag(String tag) {
			this.tag = tag;
		}

		public Object getExtra() {
			return extra==null?"":extra;
		}

		public void setExtra(Object extra) {
			this.extra = extra;
		}

	}

}
