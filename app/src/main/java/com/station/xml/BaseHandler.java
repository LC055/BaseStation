package com.station.xml;

import org.xml.sax.helpers.DefaultHandler;


/**
 * 标准SAX解析父类
 * @author hehc
 *
 */
public abstract class BaseHandler extends DefaultHandler{
    protected String currentEleName;  
    /**
     * 解析结果对象
     */
    protected XmlResult result;
	public XmlResult getResult() {
		return result;
	}
	public void setResult(XmlResult result) {
		this.result = result;
	}
    
}
 