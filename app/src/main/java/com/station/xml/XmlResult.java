package com.station.xml;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.io.StringReader;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;


//	标准返回xml格式
//	<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
//	<returnxml>
//	 <count>3</count>
//	 <returncode>200</returncode>
//	 <returnstr>成功</returnstr>
//	 <totalCount>3</totalCount>
//	<data>
//	 <detail>
//	  <字段名>字段值</字段名>
//      <字段名>
//      	<字段名>字段值<字段名>
//      	<字段名>字段值<字段名>
//      	...
//      </字段名>
//	 </detail>
//	 <detail>......</detail>
//	 <detail>......</detail>
//	</data>
//	</returnxml>

/**
 * 后台服务返回Xml数据封装<br/>
 * 标准解析器下的调用方式：XmlResult.newInstance(clazz).fromXml(String xml)
 * @author hehc
 *
 */
public class XmlResult {
	
	public static final String CODE_SUCCESS = "200";
	
	public static final String TAG_ROOT = "returnxml";
	
	public static final String TAG_RETURNCODE="returncode";
	public static final String TAG_RETURNSTR="returnstr";
	public static final String TAG_TOTALCOUNT="totalCount";//记录总数
	public static final String TAG_COUNT="count";//当前获取记录数
	public static final String TAG_DATA = "data";
	
	public static final int DEFAULT_PAGE_COUNT = 10;//分页时每页默认数量
	
	//列表数据时返回值
	private long totalCount; // 结果总记录数
	private long count;//当前页获取记录数
	
	//标准返回值
	private String returnCode;
	private String returnStr;
	
	private Data data;
	
	private String xml;//xml格式字符串
	
	private final Class<?> clazz;
	private XmlResult(Class<?> clazz){
		this.clazz = clazz;
	}
	
	public long getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(long totalCount) {
		this.totalCount = totalCount;
	}

	public long getCount() {
		return count;
	}

	public void setCount(long count) {
		this.count = count;
	}

	public Data getData() {
		if(data == null)
			data = new Data(clazz);
		return data;
	}
	
	/**
	 * 使用默认的 XmlObjectParserHandler 解析xml数据
	 * @param xml
	 * @return
	 * @throws Exception
	 */
	public XmlResult fromXml(String xml) throws Exception{
		//默认解析器
		XmlObjectParserHandler handler = new XmlObjectParserHandler();
		return this.fromXml(xml, handler);
	}

	/**
	 * 使用默认的 XmlObjectParserHandler 解析xml数据
	 * @param xml
	 * @return
	 * @throws Exception
	 */
	public XmlResult fromXml(String xml,String TAG) throws Exception{
		//默认解析器
		XmlObjectParserHandler handler=null;
		if(TAG.equals("")){
			handler = new XmlObjectParserHandler();
		}else{
			handler = new XmlObjectParserHandler(TAG);
		}
		return this.fromXml(xml, handler);
	}

	/**
	 * 使用指定的SAX解析器解析xml数据
	 * @param xml
	 * @param handler
	 * @return
	 * @throws Exception
	 */
	public XmlResult fromXml(String xml, BaseHandler handler) throws Exception{
		if(xml != null){
			this.setXml(xml);
			SAXParserFactory f = SAXParserFactory.newInstance();
			try {
				SAXParser parser = f.newSAXParser();
				handler.setResult(this);
				parser.parse(new InputSource(new StringReader(xml)), handler);
			} catch (ParserConfigurationException e) {
				e.printStackTrace();
			} catch (SAXException e) {
				e.printStackTrace();
			} catch (IOException e){
				e.printStackTrace();
			}
		}
		return this;
	}

	public String getXml() {
		return xml;
	}

	public void setXml(String xml) {
		this.xml = xml;
	}

	public String getReturnCode() {
		return returnCode;
	}

	public String getReturnStr() {
		return returnStr;
	}
	
	public void setReturnCode(String returnCode) {
		this.returnCode = returnCode;
	}

	public void setReturnStr(String returnStr) {
		this.returnStr = returnStr;
	}
	
	public boolean isSuccess(){
		if(this.getReturnCode()!= null 
				&& this.getReturnCode().equals(CODE_SUCCESS))
			return true;
		return false;
	}

	public static XmlResult newInstance(Class<?> clazz){
		return new XmlResult(clazz);
	}

}
