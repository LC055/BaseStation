package com.station.xml;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 使用此标注，配合XmlObjectParserHandler及XmlObjectHelper
 * @author hehc
 *
 */
@Target(ElementType.FIELD)     
@Retention(RetentionPolicy.RUNTIME)    
public @interface AnGetfunc{
	boolean need() default true;// 是否解析
	String name();	// 参数描述
	String viewType() default "default";//"default"说明字段为普通表单展示字段，"id" or "title" or "summary" or "tag"说明可以作为列表每行对应内容展示的字段(参考 ListItem)
	String clazz() default "default";//"default"说明字段不是自定义子对象属性，如果是子对象该处填写完整的类名，如果是集合对象填写集合元素的完整类名
	boolean kv() default true;//是否放入kv映射表（name：value）
	int kvIndex() default 0; //kv在映射表中的排序序号，为true时使用
	String Visiable() default "";
	int[] jumpTo() default { 0 };
	int pos() default 0;

}