package com.station;

import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.animation.DynamicAnimation;
import android.support.animation.SpringAnimation;
import android.support.animation.SpringForce;
import android.widget.TextView;
import android.widget.Toast;

import com.station.activity.MainTabActivity;
import com.station.databinding.ActivityEnterBinding;
import com.station.util.PreferencesUtil;

import java.util.Map;

import cn.com.cybertech.pdk.UserInfo;

public class WelComeActivity extends Activity{
	private TextView statictext;
	private ActivityEnterBinding binding;
	private SpringAnimation animation;
	
	private Map<String, String> mUserInfo;
	private Intent startintent;
	private Handler handler=new Handler(){
		public void handleMessage(Message msg) {
			//statictext.setText("");
			if(getUserInfo()){
				//mUserInfo.get("account");
				PreferencesUtil.getInstance(WelComeActivity.this).setLoginSFZH(mUserInfo.get("idcard"));
				PreferencesUtil.getInstance(WelComeActivity.this).setLastLoginPoliceCode(mUserInfo.get("account"));
				startintent = new Intent(WelComeActivity.this, MainTabActivity.class);
				//startintent.putExtra("style", "02");
				startActivity(startintent);
				finish();
			}else{
				Toast.makeText(WelComeActivity.this, "用户信息获取失败，请重新登陆", Toast.LENGTH_SHORT);
				finish();
			}
			
		};
	};
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		binding = DataBindingUtil.setContentView(WelComeActivity.this,R.layout.activity_enter);

		SpringForce spring = new SpringForce(0)
				.setDampingRatio(SpringForce.DAMPING_RATIO_LOW_BOUNCY)
				.setStiffness(SpringForce.STIFFNESS_VERY_LOW);
		animation=new SpringAnimation(binding.logoimg, DynamicAnimation.TRANSLATION_Y).setSpring(spring).setStartValue(-700);
		animation.addEndListener(new DynamicAnimation.OnAnimationEndListener() {
			@Override
			public void onAnimationEnd(DynamicAnimation animation, boolean canceled, float value, float velocity) {
                /*getStatus();
                handler.sendEmptyMessageDelayed(3, 3000);*/
				//test();
				//turn();
				handler.sendEmptyMessage(100);
			}
		});
		animation.start();

		//handler.sendEmptyMessageDelayed(100, 2*1000);
	}
	
	
	
	/**
     * 获取用户信息
     *
     * @return
     */
    private boolean getUserInfo() {
        mUserInfo = UserInfo.getUserInfo(getApplicationContext());
        if (mUserInfo != null) {
        	/*OperationLog.saveLog(WelComeActivity.this, Static.APPKEY, "登录模块", OperationLog.OperationType.CODE_LOGIN,
        			OperationLog.OperationResult.CODE_SUCCESS, OperationLog.LogType.CODE_USER_OPERATION, null);*/
            return true;
        }
        return false;
    }
	
}
