package com.station.model;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;

import com.daimajia.swipe.util.Attributes;
import com.station.R;
import com.station.activity.Tab1DetailActivity;
import com.station.adapter.Tab1ItemAdapter;
import com.station.bean.BaseBean;
import com.station.bean.JZBean;
import com.station.bean.JZListBean;
import com.station.databinding.ActivityListBinding;
import com.station.http.MNetHttp;
import com.station.listener.HttpListener;
import com.station.listener.ItemEditClick;
import com.station.listener.LoadStatusListener;
import com.station.listener.RefreshListener;
import com.station.util.BindingUtil;
import com.station.util.PreferencesUtil;
import com.station.util.RefreshUtil;
import com.station.util.Static;
import com.station.widget.RVItemDecoration;
import com.station.widget.nicespinner.NiceSpinner;
import com.station.widget.slidingmenu.SlidingMenu;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by nbzl on 2017/10/17.
 */
public class Tab1Model extends BaseModel {
    private EditText addrView,basenameView;
    private ActivityListBinding binding;
    private RefreshUtil refreshUtil;
    private LoadStatusListener statusListener;
    private Tab1ItemAdapter adapter;
    private List<Object> datalist;
    private Map<String,String> params;

    private SlidingMenu slidingMenu;
    private int page=1;
    private String basictype="";
    private Intent itemIntent;

    public Tab1Model(Context context,LoadStatusListener statusListener) {
        super(context);
        this.statusListener=statusListener;
        datalist=new ArrayList<Object>();
        params=new HashMap<String,String>();
        itemIntent=new Intent(context, Tab1DetailActivity.class);
        binding= DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.activity_list,null,false);
        //initAdapter();
        refreshUtil = new RefreshUtil(context, binding.ptrLayout, new Refresh());
        refreshUtil.initRefreshView();
        initMenu();
    }

    public void initMenu(){
        // 设置滑动菜单的属性值
        slidingMenu = new SlidingMenu(context);
        slidingMenu.setMode(SlidingMenu.RIGHT);
        slidingMenu.setTouchModeAbove(SlidingMenu.TOUCHMODE_FULLSCREEN);
        slidingMenu.setShadowWidthRes(R.dimen.shadow_width);
        //slidingMenu.setStatic(true);
        //menu.setShadowDrawable(R.drawable.shadow);
        slidingMenu.setTouchModeAbove(SlidingMenu.TOUCHMODE_NONE);
        slidingMenu.setBehindOffsetRes(R.dimen.slidingmenu_offset);
        slidingMenu.setShadowDrawable(R.drawable.shadowright);
        slidingMenu.setFadeDegree(0.35f);
        slidingMenu.attachToActivity((Activity) context, SlidingMenu.SLIDING_CONTENT);
        // 设置滑动菜单的视图界面
        slidingMenu.setMenu(R.layout.menu_tab1);
        initMenuView();
        slidingMenu.showContent();
    }
    public void initMenuView(){
        View menuView=slidingMenu.getMenu();
        final NiceSpinner spinnerjz=(NiceSpinner)menuView.findViewById(R.id.spinner_jztype);
        spinnerjz.attachDataSource(app.getBaseTypeMap().get("value"));
        spinnerjz.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Log.e("Mytext",app.getBaseTypeMap().get("key").get(position));
                basictype=app.getBaseTypeMap().get("key").get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        addrView=(EditText) menuView.findViewById(R.id.addr);
        basenameView=(EditText) menuView.findViewById(R.id.basename);

        Button submit=(Button)menuView.findViewById(R.id.searchbtn);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                page=1;
                postRequest();
                closeMenu();
                progressDialog.show();
                //statusListener.hide(true,"");
            }
        });

        Button reset=(Button)menuView.findViewById(R.id.resetbtn);
        reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addrView.setText("");
                basenameView.setText("");
                spinnerjz.setSelectedIndex(0);
                basictype="";
                page=1;
                postRequest();
                closeMenu();
                progressDialog.show();
            }
        });

    }

    private void closeMenu() {
        if (slidingMenu.isMenuShowing()) {
            slidingMenu.showContent();
        }
    }

    public void showMenu(){
        if (slidingMenu.isMenuShowing()) {
            slidingMenu.showContent();
        }
        else {
            slidingMenu.showMenu();
        }
    }

    public ActivityListBinding getBinding(){
        return binding;
    }

    public void postRequest(){
        params.put("basictype",basictype);//
        params.put("basicname", BindingUtil.isEmpty(basenameView));//
        params.put("addr",BindingUtil.isEmpty(addrView));//
        params.put("page",page+"");
        params.put("rows",Static.rows);
        params.put("policecode", PreferencesUtil.getInstance(context).getLastLoginPoliceCode());
        //Log.e("Mytext","basictype--->"+ GsonUtil.getJson(params));
        MNetHttp.getInstance().rxPostRequest(Static.BSList,params,JZListBean.class,new ListRequestListener());
    }

    public void reloadRequest(){
        page=1;
        postRequest();
    }

    public void initAdapter(){
        if(adapter==null){
            adapter=new Tab1ItemAdapter(context,datalist,R.layout.item_tab1,null,new EditClick());
            adapter.setMode(Attributes.Mode.Single);
            binding.recycleview.setAdapter(adapter);
            binding.recycleview.addItemDecoration(new RVItemDecoration(30));
            binding.recycleview.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));

        }else {
            adapter.notifyDataSetChanged();
        }
    }

    class ListRequestListener implements HttpListener{

        @Override
        public void success(Object obj) {

            //Log.e("Mytext","-->"+((JZListBean)obj).getList().get(0).getId());
            if(((JZListBean)obj).getList()==null||((JZListBean)obj).getList().size()==0){
                Static.showToast(context, context.getString(R.string.nodata));
                if (page == 1) {
                    statusListener.hide(false, "暂无更多数据");
                }
                //handler.sendEmptyMessage(100);
                binding.ptrLayout.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        binding.ptrLayout.refreshComplete();
                    }
                }, 20);
            }else{
                if (page == 1) {
                    datalist.clear();
                }
                if (((JZListBean) obj).getList().size() < Integer.valueOf(Static.rows)) {
                    Static.showToast(context, context.getString(R.string.nodata));
                }
                datalist.addAll(((JZListBean)obj).getList());
                initAdapter();
                statusListener.show();
                //Log.e("Mytext","size---->"+((JZListBean)obj).getList().get(1).getId());
                binding.ptrLayout.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        binding.ptrLayout.refreshComplete();
                    }
                },20);
            }
            progressDialog.dismiss();
        }

        @Override
        public void fail(String msg) {
            progressDialog.dismiss();
            Static.showToast(context,msg);
            statusListener.hide(false,msg);
        }
    }

    class EditClick implements ItemEditClick{

        @Override
        public void delete(int position) {
            progressDialog.show();
            Static.showToast(context,"delete-->"+position);
            Map<String,String> delparams=new HashMap<String,String>();
            delparams.put("id",((JZBean)datalist.get(position)).getId());
            delparams.put("policecode", PreferencesUtil.getInstance(context).getLastLoginPoliceCode());
            MNetHttp.getInstance().rxPostRequest(Static.DELTAB1, delparams, BaseBean.class, new HttpListener() {
                @Override
                public void success(Object obj) {
                    if(((BaseBean)obj).getReturncode().equals("200")){
                        Static.showToast(context,"删除成功");
                        page=1;
                        postRequest();

                    }else{
                        Static.showToast(context,"删除失败");
                        progressDialog.dismiss();
                    }
                }

                @Override
                public void fail(String msg) {
                    Static.showToast(context,"删除失败,msg");
                    progressDialog.dismiss();
                }
            });
        }

        @Override
        public void edit(int position) {
            //Static.showToast(context,"edit-->"+position);
            itemIntent.putExtra("id",((JZBean)datalist.get(position)).getId());
            itemIntent.putExtra("isEdit",true);
            context.startActivity(itemIntent);
        }

        @Override
        public void click(int position) {
            //Static.showToast(context,"click-->"+position);
            itemIntent.putExtra("id",((JZBean)datalist.get(position)).getId());
            itemIntent.putExtra("isEdit",false);
            context.startActivity(itemIntent);
        }
    }

    class Refresh implements RefreshListener {

        @Override
        public void refresh() {

            page=1;
            postRequest();
        }

        @Override
        public void loadmore() {


            page++;
            postRequest();
        }
    }

}
