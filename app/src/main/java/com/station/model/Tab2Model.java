package com.station.model;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.daimajia.swipe.util.Attributes;
import com.fourmob.datetimepicker.date.DatePickerDialog;
import com.station.R;
import com.station.activity.Tab2DetailActivity;
import com.station.adapter.Tab2ItemAdapter;
import com.station.bean.BaseBean;
import com.station.bean.JZBean;
import com.station.bean.XJBean;
import com.station.bean.XJListBean;
import com.station.databinding.ActivityListBinding;
import com.station.http.MNetHttp;
import com.station.listener.HttpListener;
import com.station.listener.ItemEditClick;
import com.station.listener.LoadStatusListener;
import com.station.listener.RefreshListener;
import com.station.util.BindingUtil;
import com.station.util.PreferencesUtil;
import com.station.util.RefreshUtil;
import com.station.util.Static;
import com.station.widget.RVItemDecoration;
import com.station.widget.slidingmenu.SlidingMenu;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by nbzl on 2017/10/17.
 */
public class Tab2Model extends BaseModel {
    private FragmentManager manager;
    private EditText search_name,search_xjr;
    private TextView search_xjrq;
    private ActivityListBinding binding;
    private RefreshUtil refreshUtil;
    private LoadStatusListener statusListener;
    private DatePickerDialog datePickerDialog;
    private Tab2ItemAdapter adapter;
    private List<Object> datalist;
    private Intent detailIntent;

    private SlidingMenu slidingMenu;
    private Map<String, String> params;

    private int page = 1;

    public Tab2Model(Context context,FragmentManager manager,LoadStatusListener statusListener) {
        super(context);
        this.manager=manager;
        this.statusListener = statusListener;
        datalist = new ArrayList<Object>();
        params = new HashMap<String, String>();
        detailIntent=new Intent(context, Tab2DetailActivity.class);
        binding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.activity_list, null, false);
        //initAdapter();
        refreshUtil = new RefreshUtil(context, binding.ptrLayout, new Refresh());
        refreshUtil.initRefreshView();

        Calendar calendar = Calendar.getInstance();
        datePickerDialog = DatePickerDialog.newInstance(new DataSelectListener(), calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH), true);
        datePickerDialog.setYearRange(2000, 2028);
        initMenu();
    }

    public void initMenu() {
        // 设置滑动菜单的属性值
        slidingMenu = new SlidingMenu(context);
        slidingMenu.setMode(SlidingMenu.RIGHT);
        slidingMenu.setTouchModeAbove(SlidingMenu.TOUCHMODE_FULLSCREEN);
        slidingMenu.setShadowWidthRes(R.dimen.shadow_width);
        //menu.setShadowDrawable(R.drawable.shadow);
        slidingMenu.setTouchModeAbove(SlidingMenu.TOUCHMODE_NONE);
        slidingMenu.setBehindOffsetRes(R.dimen.slidingmenu_offset);
        slidingMenu.setShadowDrawable(R.drawable.shadowright);
        slidingMenu.setFadeDegree(0.35f);
        slidingMenu.attachToActivity((Activity) context, SlidingMenu.SLIDING_CONTENT);
        // 设置滑动菜单的视图界面
        slidingMenu.setMenu(R.layout.menu_tab2);
        initMenuView();
        slidingMenu.showContent();
    }

    public void initMenuView(){
        View menuView=slidingMenu.getMenu();
        menuView.findViewById(R.id.date).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datePickerDialog.show(manager,"search_xjrq");
            }
        });
        search_name=(EditText)menuView.findViewById(R.id.basename);
        search_xjr=(EditText)menuView.findViewById(R.id.xjr);
        search_xjrq=(TextView)menuView.findViewById(R.id.xjrq);
        search_xjrq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datePickerDialog.show(manager,"search_xjrq");
            }
        });
        Button submit=(Button)menuView.findViewById(R.id.searchbtn);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                page=1;
                postRequest();
                closeMenu();
                progressDialog.show();
                //statusListener.hide(true,"");
            }
        });
        menuView.findViewById(R.id.resetbtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                search_name.setText("");
                search_xjr.setText("");
                search_xjrq.setText("");
                page=1;
                postRequest();
                closeMenu();
                progressDialog.show();
                //statusListener.hide(true,"");
            }
        });
    }

    private void closeMenu() {
        if (slidingMenu.isMenuShowing()) {
            slidingMenu.showContent();
        }
    }

    public void showMenu() {
        if (slidingMenu.isMenuShowing()) {
            slidingMenu.showContent();
        } else {
            slidingMenu.showMenu();
        }
    }

    public ActivityListBinding getBinding() {
        return binding;
    }

    public void postRequest() {
        //initAdapter();
        //statusListener.show();

        params.put("basicname", BindingUtil.isEmpty(search_name));//
        params.put("check_date",BindingUtil.isEmpty(search_xjrq));//
        params.put("check_user",BindingUtil.isEmpty(search_xjr));//
        params.put("policecode", PreferencesUtil.getInstance(context).getLastLoginPoliceCode());
        params.put("page", page + "");
        params.put("rows", Static.rows);
        MNetHttp.getInstance().rxPostRequest(Static.XJDATALIST, params, XJListBean.class, new ListRequestListener());
    }

    public void reloadRequest(){
        page=1;
        postRequest();
    }

    public void initAdapter() {
        if (adapter == null) {

            adapter = new Tab2ItemAdapter(context, datalist, R.layout.item_tab2, null, new EditClick());
            adapter.setMode(Attributes.Mode.Single);
            binding.recycleview.setAdapter(adapter);
            binding.recycleview.addItemDecoration(new RVItemDecoration(30));
            binding.recycleview.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));

        }else {
            adapter.notifyDataSetChanged();
        }
    }

    class DataSelectListener implements DatePickerDialog.OnDateSetListener {

        @Override
        public void onDateSet(DatePickerDialog datePickerDialog, int year, int month, int day) {
            Log.e("Mytext", year + "-" + month + "-" + day);
            search_xjrq.setText(year+"-"+(month+1)+"-"+day);
        }
    }

    class ListRequestListener implements HttpListener {

        @Override
        public void success(Object obj) {

            if (((XJListBean) obj).getList() == null || ((XJListBean) obj).getList().size() < Integer.valueOf(Static.rows)) {
                Static.showToast(context, context.getString(R.string.nodata));
                statusListener.hide(false, "暂无更多数据");
            } else {
                if (page == 1) {
                    datalist.clear();
                }
                datalist.addAll(((XJListBean) obj).getList());
                initAdapter();
                statusListener.show();
                //Log.e("Mytext","size---->"+((JZListBean)obj).getList().get(1).getId());
                binding.ptrLayout.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        binding.ptrLayout.refreshComplete();
                    }
                }, 20);
            }
            progressDialog.dismiss();
        }

        @Override
        public void fail(String msg) {
            Static.showToast(context,msg);
            progressDialog.dismiss();
            statusListener.hide(false, msg);
        }
    }

    class EditClick implements ItemEditClick {

        @Override
        public void delete(int position) {
            progressDialog.show();
            //Static.showToast(context,"delete-->"+position);
            Map<String,String> delparams=new HashMap<String,String>();
            delparams.put("id",((XJBean)datalist.get(position)).getId());
            delparams.put("policecode", PreferencesUtil.getInstance(context).getLastLoginPoliceCode());
            MNetHttp.getInstance().rxPostRequest(Static.DELTAB2, delparams, BaseBean.class, new HttpListener() {
                @Override
                public void success(Object obj) {
                    if(((BaseBean)obj).getReturncode().equals("200")){
                        Static.showToast(context,"删除成功");
                        page=1;
                        postRequest();

                    }else{
                        Static.showToast(context,"删除失败");
                        progressDialog.dismiss();
                    }
                }

                @Override
                public void fail(String msg) {
                    Static.showToast(context,"删除失败,msg");
                    progressDialog.dismiss();
                }
            });
        }

        @Override
        public void edit(int position) {
            //Static.showToast(context, "edit-->" + position);
            //Static.showToast(context, "click-->" + position);
            detailIntent.putExtra("id",((XJBean)(datalist.get(position))).getId());
            detailIntent.putExtra("isEdit",true);
            ((Activity)context).startActivityForResult(detailIntent,Activity.RESULT_OK);
            //((Activity)context).startActivity(detailIntent);
        }

        @Override
        public void click(int position) {
            //Static.showToast(context, "click-->" + position);
            detailIntent.putExtra("id",((XJBean)(datalist.get(position))).getId());
            detailIntent.putExtra("isEdit",false);
            ((Activity)context).startActivityForResult(detailIntent,Activity.RESULT_OK);
            //((Activity)context).startActivity(detailIntent);
        }
    }

    class Refresh implements RefreshListener {

        @Override
        public void refresh() {
            page = 1;
            postRequest();
        }

        @Override
        public void loadmore() {
            page++;
            postRequest();
        }
    }

}
