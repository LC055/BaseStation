package com.station.model;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;

import com.station.BR;
import com.station.R;
import com.station.activity.DetailActivity;
import com.station.adapter.MainItemAdapter;
import com.station.databinding.ActivityListBinding;
import com.station.listener.RecyclerItemClick;
import com.station.listener.RefreshListener;
import com.station.util.RefreshUtil;
import com.station.widget.RVItemDecoration;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nbzl on 2017/10/16.
 */
public class MainListModel extends BaseModel {

    private ActivityListBinding binding;
    private RefreshUtil refreshUtil;

    private MainItemAdapter adapter;
    private List<Object> datalist;
    private Intent itemIntent;


    public MainListModel(Context context) {
        super(context);
        binding= DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.activity_list,null,false);

        refreshUtil = new RefreshUtil(context, binding.ptrLayout, new Refresh());
        refreshUtil.initRefreshView();
        itemIntent=new Intent(context,DetailActivity.class);

        datalist=new ArrayList<Object>();
        initAdapterData();
    }

    public ActivityListBinding getBinding(){
        return binding;
    }

    public void initAdapterData(){
        for(int i=0;i<20;i++){
            datalist.add(""+i);
        }
        if(adapter==null){
            adapter=new MainItemAdapter(datalist,R.layout.item_main, BR.positionMain,new ItemClick());
            binding.recycleview.setAdapter(adapter);
            binding.recycleview.addItemDecoration(new RVItemDecoration(30));
            binding.recycleview.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
        }

    }

    class Refresh implements RefreshListener {

        @Override
        public void refresh() {
            binding.ptrLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    binding.ptrLayout.refreshComplete();
                }
            },200);

        }

        @Override
        public void loadmore() {
            binding.ptrLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                  binding.ptrLayout.refreshComplete();
                }
            },200);
        }
    }

    class ItemClick implements RecyclerItemClick{

        @Override
        public void onItemClick(int position) {
            context.startActivity(itemIntent);
        }

        @Override
        public void onItemLongClick(int position) {

        }

        @Override
        public void onMoreClick(int position) {

        }
    }
}
