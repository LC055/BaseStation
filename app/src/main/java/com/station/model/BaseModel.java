package com.station.model;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;

import com.station.App;

/**
 * Created by nbzl on 2017/8/11.
 */
public abstract class BaseModel {

    protected ProgressDialog progressDialog;
    protected final Context context;
    protected App app;

    public BaseModel(Context context){
        this.context=context;
        progressDialog=new ProgressDialog(context);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("正在加载中");
        app=(App)((Activity)context).getApplication();

    }

}
