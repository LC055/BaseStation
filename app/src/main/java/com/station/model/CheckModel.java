package com.station.model;

import android.content.Context;
import android.content.DialogInterface;
import android.databinding.DataBindingUtil;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.RadioButton;

import com.station.R;
import com.station.adapter.ExpandableListViewAdapter;
import com.station.bean.CheckBean;
import com.station.bean.PatrolEvent;
import com.station.databinding.FragmentCheckBinding;
import com.station.databinding.LayoutCheckBinding;
import com.station.util.BindingUtil;
import com.station.util.Static;

import org.greenrobot.eventbus.EventBus;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by nbzl on 2017/10/12.
 */
public class CheckModel extends BaseModel {

    private FragmentCheckBinding binding;
    private android.support.v7.app.AlertDialog.Builder builder;
    private ExpandableListViewAdapter adapter;

    private List<CheckBean.ListBeanX> datalist;
    private boolean isEdit;
    public Handler handler=new Handler(){
        @Override
        public void handleMessage(Message msg) {
            adapter.notifyDataSetChanged();
        }
    };

    public CheckModel(Context context,boolean isEdit) {
        super(context);
        this.isEdit=isEdit;
        binding= DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.fragment_check,null,false);

    }

    public FragmentCheckBinding getBinding(){
        return binding;
    }

    public void showDialog(CheckBean.ListBeanX.ListBean bean,final int groupPosition,final int childPosition){
        builder=new AlertDialog.Builder(context);
        builder.setTitle("检测");
        final LayoutCheckBinding checkBinding=DataBindingUtil.inflate(LayoutInflater.from(context),R.layout.layout_check,null,false);

        checkBinding.setData(bean);
        if (bean.getIsnormal()==null){
            checkBinding.isnormal.setChecked(false);
            checkBinding.isunnormal.setChecked(false);
        }else if(bean.getIsnormal().equals("1")){
            checkBinding.isnormal.setChecked(true);
        }else if(bean.getIsnormal().equals("0")){
            checkBinding.isunnormal.setChecked(true);
        }

        if(bean.getIssolve()==null){
            checkBinding.issolve.setChecked(false);
            checkBinding.isunsolve.setChecked(false);
        }else if(bean.getIssolve().equals("1")){
            checkBinding.issolve.setChecked(true);
        }else if(bean.getIssolve().equals("0")){
            checkBinding.isunsolve.setChecked(true);
        }

        builder.setView(checkBinding.getRoot());
        builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                builder=null;
            }
        });
        builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //checkBinding.
                if(checkBinding.isnormal.isChecked()){
                    (datalist.get(groupPosition).getList().get(childPosition)).setIsnormal("1");
                }else if(checkBinding.isunnormal.isChecked()){
                    (datalist.get(groupPosition).getList().get(childPosition)).setIsnormal("0");
                }else{
                    (datalist.get(groupPosition).getList().get(childPosition)).setIsnormal("");
                }

                if(checkBinding.issolve.isChecked()){
                    (datalist.get(groupPosition).getList().get(childPosition)).setIssolve("1");
                }else if(checkBinding.isunsolve.isChecked()){
                    (datalist.get(groupPosition).getList().get(childPosition)).setIssolve("0");
                }else{
                    (datalist.get(groupPosition).getList().get(childPosition)).setIssolve("");
                }
                (datalist.get(groupPosition).getList().get(childPosition)).setCheckremark(BindingUtil.isEmpty(checkBinding.remark));
                handler.sendEmptyMessage(100);
                builder=null;
            }
        });
        builder.show();
    }


    public void setModel(List<CheckBean.ListBeanX> checklist){
        datalist=checklist;
        if(adapter==null){
            adapter=new ExpandableListViewAdapter(context,datalist);
            binding.expandlistview.setAdapter(adapter);
            binding.expandlistview.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
                @Override
                public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                    //Static.showToast(content,);
                    if(isEdit){
                        showDialog(datalist.get(groupPosition).getList().get(childPosition),groupPosition,childPosition);
                    }
                    return false;
                }
            });
        }
    }

    public void getParams() {
        Map<String, String> params = new HashMap<String, String>();
        for(int i=0,k=0;i<datalist.size();i++){

            for(int j=0;j<datalist.get(i).getList().size();j++){
                params.put("list["+k+"].isnormal",datalist.get(i).getList().get(j).getIsnormal());
                params.put("list["+k+"].issolve",datalist.get(i).getList().get(j).getIssolve());
                params.put("list["+k+"].checkremark",datalist.get(i).getList().get(j).getCheckremark());
                params.put("list["+k+"].checksorid",datalist.get(i).getList().get(j).getId());
                k++;
            }
        }
        PatrolEvent event=new PatrolEvent(params);
        EventBus.getDefault().post(event);
    }

    public class ViewClickListener{
        public void onClick(View v) {
            /*switch (v.getId()){
                case R.id.check11:
                    showDialog(binding.statue11,binding.remark11);
                    break;
                case R.id.check12:
                    showDialog(binding.statue12,binding.remark12);
                    break;
                case R.id.check21:
                    showDialog(binding.statue21,binding.remark21);
                    break;

                case R.id.select1:
                    binding.expandlayout1.toggle();
                    //expandStatus(v,binding.expandlayout1);
                    break;
                case R.id.select2:
                    binding.expandlayout2.toggle();
                    break;
            }*/
        }
    }

}
