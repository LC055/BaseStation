package com.station.model;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;

import com.fourmob.datetimepicker.date.DatePickerDialog;
import com.sleepbot.datetimepicker.time.RadialPickerLayout;
import com.sleepbot.datetimepicker.time.TimePickerDialog;
import com.station.R;
import com.station.bean.BaseTypeBean;
import com.station.bean.PatrolEvent;
import com.station.bean.ZXJBean;
import com.station.databinding.ActivityEditBinding;
import com.station.util.BindingUtil;
import com.station.util.GsonUtil;
import com.station.util.PreferencesUtil;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by nbzl on 2017/10/9.
 */
public class EditModel extends BaseModel {
    private ActivityEditBinding binding;
    private FragmentManager manager;
    private TimePickerDialog timePickerDialog;
    private DatePickerDialog datePickerDialog;
    private String basicid,deptcode,deptname,replay_date,type,queryStart,queryEnd,id="";
    private int year_e = 0, month_e = 0, day_e = 0;
    private boolean isedit;

    public EditModel(Context context, FragmentManager manager, boolean isedit) {
        super(context);
        this.isedit = isedit;
        this.manager = manager;
        binding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.activity_edit, null, false);
        binding.setIsEdit(this.isedit);
        binding.setClick(new ViewClick());
        binding.setData(new ZXJBean.DataBean());

        Calendar calendar = Calendar.getInstance();
        timePickerDialog = TimePickerDialog.newInstance(new TimeSelectListener(), calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), true);
        datePickerDialog = DatePickerDialog.newInstance(new DataSelectListener(), calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH), true);
        datePickerDialog.setYearRange(2000, 2028);

    }

    public ActivityEditBinding getBinder() {
        return binding;
    }

    public void setModel(ZXJBean zxjBean) {
        id=zxjBean.getData().getId();
        basicid = zxjBean.getData().getBasicid();
        deptcode=zxjBean.getData().getDeptcode();
        deptname=zxjBean.getData().getDeptname();
        replay_date=zxjBean.getData().getReplay_date();
        type=zxjBean.getData().getType();
        queryStart=zxjBean.getData().getQueryStart();
        queryEnd=zxjBean.getData().getQueryEnd();

        binding.setData(zxjBean.getData());
        binding.setIsEdit(isedit);
    }

    public void setSpinnerData(List<BaseTypeBean> baseList){
        List<String> value=new ArrayList<String>();
        final List<String> key=new ArrayList<String>();
        int select=0;
        for(int i=0;i<baseList.size();i++){
            value.add(baseList.get(i).getText());
            key.add(baseList.get(i).getCode());
            if(basicid!=null&&basicid.equals(baseList.get(i).getCode())){
                select=i;
            }
        }
        //basicid=key.get(0);
        binding.spinnerjztype.attachDataSource(value);
        binding.spinnerjztype.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                basicid=key.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        Log.e("Mytext","select--->"+select);
        binding.spinnerjztype.setSelectedIndex(select);
        basicid=key.get(select);
    }

    public void getParams() {

        Map<String, String> params = new HashMap<String, String>();

        params.put("id",id);
        params.put("basicname", BindingUtil.isEmpty(binding.basicname));
        params.put("check_user", BindingUtil.isEmpty(binding.checkuser));
        params.put("wendu", BindingUtil.isEmpty(binding.wendu));
        params.put("shidu", BindingUtil.isEmpty(binding.shidu));
        params.put("check_date",BindingUtil.isEmpty(binding.checkdate));
        params.put("remark", BindingUtil.isEmpty(binding.remark));

        params.put("basicid", BindingUtil.isEmpty(basicid));
        params.put("deptcode", BindingUtil.isEmpty(deptcode));
        params.put("deptname", BindingUtil.isEmpty(deptname));
        params.put("replay_date", BindingUtil.isEmpty(replay_date));
        params.put("type", BindingUtil.isEmpty(type));
        params.put("queryStart", BindingUtil.isEmpty(queryStart));//
        params.put("queryEnd", BindingUtil.isEmpty(queryEnd));
        params.put("policecode", PreferencesUtil.getInstance(context).getLastLoginPoliceCode());
        //backListener.getBackMap(params);
        Log.e("Mytext", "editmodel-->"+GsonUtil.getJson(params));
        PatrolEvent event = new PatrolEvent(params);

        EventBus.getDefault().post(event);
    }

    class DataSelectListener implements DatePickerDialog.OnDateSetListener {

        @Override
        public void onDateSet(DatePickerDialog datePickerDialog, int year, int month, int day) {
            Log.e("Mytext", year + "-" + month + "-" + day);
            year_e = year;
            month_e = month+1;
            day_e = day;

            timePickerDialog.show(manager, "timePickerDialog");
        }
    }

    class TimeSelectListener implements TimePickerDialog.OnTimeSetListener {

        @Override
        public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute) {
            binding.checkdate.setText(year_e + "-" + month_e + "-" + day_e + " " + hourOfDay + ":" + minute);
        }
    }

    public class ViewClick {
        public void OnClick(View view) {
            switch (view.getId()) {
                case R.id.checkdate:
                    if (isedit) {
                        datePickerDialog.show(manager, "createTime");
                    }
                    break;
            }
        }
    }
}
