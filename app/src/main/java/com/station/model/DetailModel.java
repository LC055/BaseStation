package com.station.model;

import android.app.Activity;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;

import com.station.R;
import com.station.bean.BaseTypeBean;
import com.station.bean.CheckBean;
import com.station.bean.ZXJBean;
import com.station.databinding.ActivityDetailBinding;
import com.station.fragment.AddImgFragment;
import com.station.fragment.CheckFragment;
import com.station.fragment.EditFragment;
import com.station.http.MNetHttp;
import com.station.listener.LoadStatusListener;
import com.station.listener.RxHttpListener;
import com.station.util.GsonUtil;
import com.station.util.PreferencesUtil;
import com.station.util.Static;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by nbzl on 2017/10/12.
 */
public class DetailModel extends BaseModel{
    private ActivityDetailBinding binding;
    private EditFragment editFragment;
    private CheckFragment checkFragment;
    private AddImgFragment addImgFragment;
    private FragmentManager manager;

    private ZXJBean zxjBean=null;
    private CheckBean checkBean = null;
    private List<BaseTypeBean> baseList=null;

    public LoadStatusListener loadStatus;
    public Map<String, String> params, checkparams;
    private int current = -1;


    public DetailModel(Context context, FragmentManager manager,boolean isEdit,LoadStatusListener loadStatus) {
        super(context);
        this.manager = manager;
        this.loadStatus=loadStatus;
        params = new HashMap<String, String>();
        checkparams = new HashMap<String, String>();
        binding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.activity_detail, null, false);

        Bundle bundle=new Bundle();
        bundle.putBoolean("isEdit",isEdit);
        editFragment = new EditFragment(manager);
        editFragment.setArguments(bundle);
        checkFragment = new CheckFragment();
        checkFragment.setArguments(bundle);
        addImgFragment = new AddImgFragment();
        addImgFragment.setArguments(bundle);

        binding.setClick(new ViewClickListener());

        manager.beginTransaction().add(R.id.fragmentcontent, editFragment, "editFragment").add(R.id.fragmentcontent, checkFragment, "checkFragment").add(R.id.fragmentcontent, addImgFragment, "addImgFragment").commit();

        EventBus.getDefault().register(context);

        if(((Activity) context).getIntent()==null||((Activity) context).getIntent().getStringExtra("id")==null){
            progressDialog.show();
            List<Map<String, Object>> datalist = new ArrayList<Map<String, Object>>();
            Map<String,String> map=new HashMap<String,String>();
            map.put("policecode",PreferencesUtil.getInstance(context).getLastLoginPoliceCode());
            Map<String, Object> map1 = new HashMap<String, Object>();
            map1.put("url", Static.BASELIST);
            map1.put("param", map);
            datalist.add(map1);

            Map<String, Object> map2 = new HashMap<String, Object>();
            map2.put("url", Static.CheckList);
            map2.put("param", map);
            datalist.add(map2);


            MNetHttp.getInstance().rxListRequest(datalist, new AddResultListener());
        }else{
            //loadStatus.hide(true,"");
            binding.fragmentcontent.setVisibility(View.INVISIBLE);
            progressDialog.show();
            postRequest();
        }

    }



    public void upRequest(Map<String,String> params){
        params.clear();
        editFragment.getModel().getParams();
        checkFragment.getModel().getParams();
        addImgFragment.getModel().getParams();
    }

    public void initFragment(int current) {
        if (this.current != current) {
            this.current = current;
            if (current == 0) {
                binding.basicBtn.setBackground(ContextCompat.getDrawable(context, R.drawable.selectlabel_normal));
                binding.basicBtn.setTextColor(ContextCompat.getColor(context, R.color.grey_color4));

                binding.checkBtn.setBackground(ContextCompat.getDrawable(context, R.drawable.selectlabel_focused));
                binding.upBtn.setBackground(ContextCompat.getDrawable(context, R.drawable.selectlabel_focused));
                binding.upBtn.setTextColor(ContextCompat.getColor(context, R.color.level_one_text));
                binding.checkBtn.setTextColor(ContextCompat.getColor(context, R.color.level_one_text));
                manager.beginTransaction().hide(checkFragment).hide(addImgFragment).show(editFragment).commit();
                //manager.beginTransaction().replace(R.id.fragmentcontent,editFragment).commit();
            } else if (current == 1) {
                binding.checkBtn.setBackground(ContextCompat.getDrawable(context, R.drawable.selectlabel_normal));
                binding.checkBtn.setTextColor(ContextCompat.getColor(context, R.color.grey_color4));

                binding.basicBtn.setBackground(ContextCompat.getDrawable(context, R.drawable.selectlabel_focused));
                binding.upBtn.setBackground(ContextCompat.getDrawable(context, R.drawable.selectlabel_focused));
                binding.upBtn.setTextColor(ContextCompat.getColor(context, R.color.level_one_text));
                binding.basicBtn.setTextColor(ContextCompat.getColor(context, R.color.level_one_text));
                manager.beginTransaction().hide(editFragment).hide(addImgFragment).show(checkFragment).commit();
                //manager.beginTransaction().replace(R.id.fragmentcontent,checkFragment).commit();
            } else if (current == 2) {
                binding.upBtn.setBackground(ContextCompat.getDrawable(context, R.drawable.selectlabel_normal));
                binding.upBtn.setTextColor(ContextCompat.getColor(context, R.color.grey_color4));

                binding.checkBtn.setBackground(ContextCompat.getDrawable(context, R.drawable.selectlabel_focused));
                binding.basicBtn.setBackground(ContextCompat.getDrawable(context, R.drawable.selectlabel_focused));
                binding.checkBtn.setTextColor(ContextCompat.getColor(context, R.color.level_one_text));
                binding.basicBtn.setTextColor(ContextCompat.getColor(context, R.color.level_one_text));
                manager.beginTransaction().hide(editFragment).hide(checkFragment).show(addImgFragment).commit();
                //manager.beginTransaction().replace(R.id.fragmentcontent,addImgFragment).commit();
            }

        }
    }

    public void postRequest() {
        loadStatus.hide(true,"正在加载");
        params.put("id", ((Activity) context).getIntent().getStringExtra("id"));
        params.put("policecode",PreferencesUtil.getInstance(context).getLastLoginPoliceCode());


        List<Map<String, Object>> datalist = new ArrayList<Map<String, Object>>();
        Map<String, Object> map1 = new HashMap<String, Object>();

        map1.put("url", Static.TAB2DETAIL);
        map1.put("param", params);
        datalist.add(map1);



        Map<String, Object> map2 = new HashMap<String, Object>();
        map2.put("url", Static.TAB22DETAIL);
        map2.put("param", params);
        datalist.add(map2);

        Map<String, Object> map3 = new HashMap<String, Object>();
        map3.put("url", Static.BASELIST);
        map3.put("param", params);
        datalist.add(map3);

        MNetHttp.getInstance().rxListRequest(datalist, new ResultListener());
    }


    public class ViewClickListener {
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.basicBtn:
                    initFragment(0);
                    break;
                case R.id.checkBtn:
                    initFragment(1);
                    break;
                case R.id.upBtn:
                    initFragment(2);
                    break;
            }
        }
    }

    public ActivityDetailBinding getBinding() {
        return binding;
    }

    class ResultListener implements RxHttpListener {


        @Override
        public void finish() {
            binding.fragmentcontent.setVisibility(View.VISIBLE);
            progressDialog.dismiss();
            //loadStatus.show();
        }

        @Override
        public void fail(String msg) {
            binding.fragmentcontent.setVisibility(View.VISIBLE);
            progressDialog.dismiss();
            Static.showToast(context,msg);
            //loadStatus.hide(false,""+msg);
        }

        @Override
        public void next(Object obj) {
            Log.e("Mytext","ResultListener--->"+obj.toString());
            if (zxjBean == null) {
                zxjBean=GsonUtil.getInstance().getObject(obj.toString(), ZXJBean.class);
                editFragment.setModel(zxjBean);
                zxjBean.getData().getFjlist();
                //"list[0].isnormal"
                addImgFragment.setModel(zxjBean.getData().getFjlist());
            }else if (checkBean == null) {
                checkBean=GsonUtil.getInstance().getObject(obj.toString(), CheckBean.class);
                checkFragment.setModel(checkBean.getList());

            }else if(baseList==null){
                baseList=GsonUtil.getInstance().getArray(obj.toString(),BaseTypeBean[].class);
                Log.e("Mytext","size-->"+baseList.size());
                editFragment.setSpinnerData(baseList);
                loadStatus.show();
                initFragment(0);
            }

        }
    }

    class AddResultListener implements RxHttpListener {


        @Override
        public void finish() {
            progressDialog.dismiss();
        }

        @Override
        public void fail(String msg) {
            progressDialog.dismiss();
            Static.showToast(context,msg);
        }

        @Override
        public void next(Object obj) {
            Log.e("Mytext","ResultListener--->"+obj.toString());
            if(baseList==null){
                baseList=GsonUtil.getInstance().getArray(obj.toString(),BaseTypeBean[].class);
                Log.e("Mytext","size-->"+baseList.size());
                editFragment.setSpinnerData(baseList);

            }else{
                List<CheckBean.ListBeanX> checkResult=GsonUtil.getInstance().getObject(obj.toString(),CheckBean.class).getList();
                for(CheckBean.ListBeanX bean:checkResult){
                    for(CheckBean.ListBeanX.ListBean l:bean.getList()){
                        l.setIsnormal("1");
                    }
                }
                checkFragment.setModel(checkResult);
                loadStatus.show();
                initFragment(0);
            }

        }
    }

    public void onDestroy(){
        EventBus.getDefault().unregister(context);
    }

}
