package com.station.model;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.databinding.DataBindingUtil;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.fourmob.datetimepicker.date.DatePickerDialog;
import com.station.R;
import com.station.adapter.Tab3ItemAdapter;
import com.station.bean.WXJBean;
import com.station.bean.WXJListBean;
import com.station.databinding.ActivityListBinding;
import com.station.databinding.LayoutWxjBinding;
import com.station.http.MNetHttp;
import com.station.listener.HttpListener;
import com.station.listener.LoadStatusListener;
import com.station.listener.RecyclerItemClick;
import com.station.listener.RefreshListener;
import com.station.util.BindingUtil;
import com.station.util.GsonUtil;
import com.station.util.PreferencesUtil;
import com.station.util.RefreshUtil;
import com.station.util.Static;
import com.station.widget.RVItemDecoration;
import com.station.widget.slidingmenu.SlidingMenu;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by nbzl on 2017/10/17.
 */
public class Tab3Model extends BaseModel {
    private EditText basename;
    private ActivityListBinding binding;
    private RefreshUtil refreshUtil;
    private LoadStatusListener statusListener;
    private Tab3ItemAdapter adapter;
    private List<Object> datalist;
    private Map<String, String> params;
    private DatePickerDialog datePickerDialog;
    private SlidingMenu slidingMenu;
    private FragmentManager manager;
    private TextView startTime, endTime;

    private android.support.v7.app.AlertDialog.Builder builder;

    private int page = 1;

    public Tab3Model(Context context, FragmentManager manager, LoadStatusListener statusListener) {
        super(context);
        this.statusListener = statusListener;
        this.manager = manager;
        datalist = new ArrayList<Object>();
        params = new HashMap<String, String>();
        binding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.activity_list, null, false);
        //initAdapter();
        refreshUtil = new RefreshUtil(context, binding.ptrLayout, new Refresh());
        refreshUtil.initRefreshView();

        Calendar calendar = Calendar.getInstance();
        datePickerDialog = DatePickerDialog.newInstance(new DataSelectListener(), calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH), true);
        datePickerDialog.setYearRange(2000, 2028);

        initMenu();
    }

    public void initMenu() {
        // 设置滑动菜单的属性值
        slidingMenu = new SlidingMenu(context);
        slidingMenu.setMode(SlidingMenu.RIGHT);
        slidingMenu.setTouchModeAbove(SlidingMenu.TOUCHMODE_FULLSCREEN);
        slidingMenu.setShadowWidthRes(R.dimen.shadow_width);
        //menu.setShadowDrawable(R.drawable.shadow);
        slidingMenu.setTouchModeAbove(SlidingMenu.TOUCHMODE_NONE);
        slidingMenu.setBehindOffsetRes(R.dimen.slidingmenu_offset);
        slidingMenu.setShadowDrawable(R.drawable.shadowright);
        slidingMenu.setFadeDegree(0.35f);
        slidingMenu.attachToActivity((Activity) context, SlidingMenu.SLIDING_CONTENT);
        // 设置滑动菜单的视图界面
        slidingMenu.setMenu(R.layout.menu_tab3);
        initMenuView();
        slidingMenu.showContent();
    }

    public void showItemContent(WXJBean bean) {
        builder = new AlertDialog.Builder(context);
        builder.setTitle("详情");
        LayoutWxjBinding wxjBinding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.layout_wxj, null, false);
        wxjBinding.setData(bean);
        builder.setView(wxjBinding.getRoot());
        builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                builder = null;
            }
        });
        builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //checkBinding.

                builder = null;
            }
        });
        builder.show();
    }

    public void initMenuView() {
        View menuView = slidingMenu.getMenu();
        basename = (EditText) menuView.findViewById(R.id.basename);
        startTime = (TextView) menuView.findViewById(R.id.start_time);
        startTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datePickerDialog.show(manager, "starttime");
            }
        });
        endTime = (TextView) menuView.findViewById(R.id.end_time);
        endTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datePickerDialog.show(manager, "endtime");
            }
        });
        menuView.findViewById(R.id.submitbtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                page = 1;
                postRequest();
                closeMenu();
                progressDialog.show();
            }
        });
        menuView.findViewById(R.id.resetbtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                basename.setText("");
                endTime.setText("");
                startTime.setText("");
                page = 1;
                postRequest();
                closeMenu();
                progressDialog.show();
            }
        });

        menuView.findViewById(R.id.start_img).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datePickerDialog.show(manager, "starttime");
            }
        });


        menuView.findViewById(R.id.end_img).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datePickerDialog.show(manager, "endtime");
            }
        });


    }

    private void closeMenu() {
        if (slidingMenu.isMenuShowing()) {
            slidingMenu.showContent();
        }
    }

    public void showMenu() {
        if (slidingMenu.isMenuShowing()) {
            slidingMenu.showContent();
        } else {
            slidingMenu.showMenu();
        }
    }

    public ActivityListBinding getBinding() {
        return binding;
    }

    public void postRequest() {
        /*initAdapter();
        statusListener.show();*/
        //params.put("check_start_time", BindingUtil.isEmpty(startTime));
        //params.put("check_end_time",BindingUtil.isEmpty(endTime));
        params.put("basicname", BindingUtil.isEmpty(basename));
        params.put("policecode", PreferencesUtil.getInstance(context).getLastLoginPoliceCode());
        params.put("page", page + "");
        params.put("rows", Static.rows);

        Log.e("Mytext","json-->"+ GsonUtil.getJson(params));
        MNetHttp.getInstance().rxPostRequest(Static.XJLIST, params, WXJListBean.class, new ListRequestListener());
    }

    public void initAdapter() {
        if (adapter == null) {
            adapter = new Tab3ItemAdapter(datalist, R.layout.item_tab3, 0, new RecycleClick());
            binding.recycleview.setAdapter(adapter);
            binding.recycleview.addItemDecoration(new RVItemDecoration(30));
            binding.recycleview.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));

        } else {
            adapter.notifyDataSetChanged();
        }
    }


    class DataSelectListener implements DatePickerDialog.OnDateSetListener {

        @Override
        public void onDateSet(DatePickerDialog datePickerDialog, int year, int month, int day) {
            if (datePickerDialog.getTag().equals("starttime")) {
                startTime.setText(year + "-" + (month + 1) + "-" + day);
            } else if (datePickerDialog.getTag().equals("endtime")) {
                endTime.setText(year + "-" + (month + 1) + "-" + day);
            }
        }
    }

    class ListRequestListener implements HttpListener {

        @Override
        public void success(Object obj) {

            if (((WXJListBean) obj).getList() == null || ((WXJListBean) obj).getList().size() == 0) {
                Static.showToast(context, context.getString(R.string.nodata));
                if (page == 1) {
                    statusListener.hide(false, "暂无更多数据");
                }
                binding.ptrLayout.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        binding.ptrLayout.refreshComplete();
                    }
                }, 20);
            } else {
                if (page == 1) {
                    datalist.clear();
                }
                if (((WXJListBean) obj).getList().size() < Integer.valueOf(Static.rows)) {
                    Static.showToast(context, context.getString(R.string.nodata));
                }
                datalist.addAll(((WXJListBean) obj).getList());
                initAdapter();
                statusListener.show();
                //Log.e("Mytext","size---->"+((JZListBean)obj).getList().get(1).getId());
                binding.ptrLayout.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        binding.ptrLayout.refreshComplete();
                    }
                }, 20);
                progressDialog.dismiss();
            }
        }

        @Override
        public void fail(String msg) {
            progressDialog.dismiss();
            Static.showToast(context,msg);
        }
    }

    class RecycleClick implements RecyclerItemClick {


        @Override
        public void onItemClick(int position) {
            //Static.showToast(context, "" + position);
            //WXJBean bean=(WXJBean)datalist.get(position);
            showItemContent((WXJBean) datalist.get(position));
        }

        @Override
        public void onItemLongClick(int position) {

        }

        @Override
        public void onMoreClick(int position) {

        }
    }

    class Refresh implements RefreshListener {

        @Override
        public void refresh() {
            page = 1;
            postRequest();

        }

        @Override
        public void loadmore() {
            page++;
            postRequest();
        }
    }

}
