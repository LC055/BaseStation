package com.station.model;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.animation.SpringAnimation;
import android.support.v7.widget.GridLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;

import com.station.R;
import com.station.adapter.GridlayoutAdapter;
import com.station.bean.FileEvent;
import com.station.bean.ZXJBean;
import com.station.databinding.FragmentImgBinding;
import com.station.listener.GralleryImgListener;
import com.station.listener.LoadImageListener;
import com.station.listener.RecyclerItemClick;
import com.station.util.DensityUtil;
import com.station.util.GlideHelper;
import com.station.util.GralleryImgUtil;
import com.station.util.Static;
import com.station.widget.GridItemDecoration;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by nbzl on 2017/10/13.
 */
public class AddImgModel extends BaseModel {

    private SpringAnimation animation_hide, animation_show;
    private FragmentImgBinding binding;
    private int margin = 80, width, count = 4;
    private List<ImageView> imgviewList;
    private List<Object> datalist;
    private int imgcount = 0;
    private GridlayoutAdapter adapter;

    private android.support.v7.app.AlertDialog.Builder imgdel;

    public AddImgModel(Context context,boolean isEdit) {
        super(context);
        imgviewList = new ArrayList<ImageView>();
        datalist = new ArrayList<Object>();

        binding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.fragment_img, null, false);
        binding.setClick(new ViewClickListener());
        binding.setIsEdit(isEdit);
        binding.setIsEmpty(false);
        width = DensityUtil.getScreenWidth(context) / count - DensityUtil.px2dip(context, 10) - DensityUtil.dip2px(context, 2);
    }

    public FragmentImgBinding getBinding() {
        return binding;
    }

    public void setModel(List<ZXJBean.FJBean> beanlist) {
        if(beanlist==null||beanlist.size()==0){
            binding.setIsEmpty(true);
        }else{
            binding.setIsEmpty(false);
        }
        if(beanlist!=null){
            imgcount = beanlist.size();
            for (int i = 0; i < beanlist.size(); i++) {
                GlideHelper.getInstance(context).downLoadImg(beanlist.get(i).getFjmc(), Static.GETIMG+beanlist.get(i).getFjurl(), beanlist.get(i), new DownImageListener());
            }
        }
                /*for(ZXJBean.FJBean bean:beanlist){

            GlideHelper.getInstance(context).downLoadImg(bean.getFjmc(),Static.GETIMG+bean.getFjurl(),new DownImageListener());
        }*/
    }

    public class ViewClickListener {
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.addImg:
                    if(datalist.size()<6){
                        GralleryImgUtil.getInstance(context, new GralleryimgListener()).showImgList();
                    }else{
                        Static.showToast(context,"图片数量不能超过6张");
                    }

                    /*if(binding.contentImg.getChildCount()>=4){
                        Static.showToast(context,"最多只能上传4张照片");
                    }else{

                    }*/

                    break;
            }
        }
    }


    public void getParams() {
        if(datalist.size()<3){
            Static.showToast(context,"上传的图片不能小于3张");
            return;
        }else{
            List<String> imglist = new ArrayList<String>();
            for (Object map : datalist) {
                if (((Map<String, Object>) map).get("data") == null) {
                    imglist.add(((Map<String, Object>) map).get("path").toString());
                }
            }
            FileEvent event = new FileEvent(imglist);
            EventBus.getDefault().post(event);
        }

    }

    class DownImageListener implements LoadImageListener {

        @Override
        public void downloadImg(String filepath, Object object) {
            imgcount--;
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("path", filepath);
            map.put("data", object);
            datalist.add(map);
            if (imgcount == 0 && adapter == null) {
                adapter = new GridlayoutAdapter(context,datalist, R.layout.grid_img, 0, new RecycleClick());
                binding.recycleview.setLayoutManager(new GridLayoutManager(context, 4));
                binding.recycleview.setAdapter(adapter);
                binding.recycleview.addItemDecoration(new GridItemDecoration(10));
            }

            /*Log.e("Mytext",filepath);
            float scale=Float.valueOf(BitmapFactory.decodeFile(filepath).getHeight()+"")/Float.valueOf(BitmapFactory.decodeFile(filepath).getWidth()+"");
            ImageView imgview = new ImageView(context);
            imgview.setLayoutParams(new ViewGroup.LayoutParams(width, (int)(scale*Float.valueOf(""+width))));
            imgview.setPadding(1, 0, 1, 0);
            imgview.setImageBitmap(BitmapFactory.decodeFile(filepath));
            binding.contentImg.addView(imgview);*/
        }

        @Override
        public void failload() {
            imgcount--;
            Map<String, Object> data = new HashMap<String, Object>();
            data.put("path", "");
            data.put("data", null);
            datalist.add(data);
            if(adapter==null){
                adapter = new GridlayoutAdapter(context,datalist, R.layout.grid_img, 0, new RecycleClick());
                binding.recycleview.setLayoutManager(new GridLayoutManager(context, 4));
                binding.recycleview.setAdapter(adapter);
                binding.recycleview.addItemDecoration(new GridItemDecoration(10));
            }else{
                adapter.notifyDataSetChanged();
            }
        }
    }

    class RecycleClick implements RecyclerItemClick {

        @Override
        public void onItemClick(int position) {

        }

        @Override
        public void onItemLongClick(int position) {

        }

        @Override
        public void onMoreClick(int position) {

        }
    }

    class GralleryimgListener implements GralleryImgListener {

        @Override
        public void success(String filepath) {
            Map<String, Object> data = new HashMap<String, Object>();
            data.put("path", filepath);
            data.put("data", null);
            datalist.add(data);
            if(adapter==null){
                adapter = new GridlayoutAdapter(context,datalist, R.layout.grid_img, 0, new RecycleClick());
                binding.recycleview.setLayoutManager(new GridLayoutManager(context, 4));
                binding.recycleview.setAdapter(adapter);
                binding.recycleview.addItemDecoration(new GridItemDecoration(10));
            }else{
                adapter.notifyDataSetChanged();
            }

        }

        @Override
        public void fail(String msg) {

        }
    }


}
