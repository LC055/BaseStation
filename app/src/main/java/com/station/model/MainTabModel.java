package com.station.model;

import android.content.Context;
import android.content.res.ColorStateList;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.MenuItem;

import com.station.R;
import com.station.activity.MainTabActivity;
import com.station.adapter.ViewPagerAdapter;
import com.station.databinding.ActivityTabBinding;
import com.station.fragment.Tab1Fragment;
import com.station.fragment.Tab2Fragment;
import com.station.fragment.Tab3Fragment;
import com.station.fragment.Tab4Fragment;
import com.station.util.BottomNavigationViewHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nbzl on 2017/10/17.
 */
public class MainTabModel extends BaseModel {
    private ActivityTabBinding binding;
    private ViewPagerAdapter adapter;
    private List<Fragment> fragments;

    private int[] colors = new int[2];

    public MainTabModel(Context context, FragmentManager manager) {
        super(context);
        fragments = new ArrayList<Fragment>();
        int[][] states = new int[][]{new int[]{-android.R.attr.state_checked}, new int[]{android.R.attr.state_checked}};
        colors[0] = ContextCompat.getColor(context, R.color.black_light2);
        colors[1] = ContextCompat.getColor(context, R.color.colorPrimary);
        ColorStateList csl = new ColorStateList(states, colors);

        binding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.activity_tab, null, false);
        binding.navigationbar.setItemTextColor(csl);
        binding.navigationbar.setItemIconTintList(csl);
        BottomNavigationViewHelper.disableShiftMode(binding.navigationbar);

        binding.navigationbar.setOnNavigationItemSelectedListener(new BottomTabClick());

        fragments.add(new Tab1Fragment());
        fragments.add(new Tab2Fragment(manager));
        fragments.add(new Tab3Fragment(manager));
        fragments.add(new Tab4Fragment());
        adapter = new ViewPagerAdapter(manager, fragments);
        binding.viewpager.setAdapter(adapter);
        binding.viewpager.setOffscreenPageLimit(4);
    }

    public void menuClick() {
        switch (binding.viewpager.getCurrentItem()) {
            case 0:
                ((Tab1Fragment)fragments.get(0)).showMenu();
                break;
            case 1:
                ((Tab2Fragment)fragments.get(1)).showMenu();
                break;
            case 2:
                ((Tab3Fragment)fragments.get(2)).showMenu();
                break;
            case 3:
                ((Tab4Fragment)fragments.get(3)).showMenu();
                break;
        }
    }

    public void menuAddClick() {
        switch (binding.viewpager.getCurrentItem()) {
            case 0:
                ((Tab1Fragment)fragments.get(0)).menuAddClick();
                break;
            case 1:
                ((Tab2Fragment)fragments.get(1)).menuAddClick();
                break;
            /*case 2:
                ((Tab3Fragment)fragments.get(2)).showMenu();
                break;*/
            case 3:
                ((Tab4Fragment)fragments.get(3)).menuAddClick();
                break;
        }
    }

    public void refreshFragment(){
        switch (binding.viewpager.getCurrentItem()){
            case 0:
                ((Tab1Fragment)fragments.get(0)).reloadData();
                break;
            case 1:
                ((Tab2Fragment)fragments.get(1)).reloadData();
                break;
            case 2:

                break;
            case 3:
                ((Tab4Fragment)fragments.get(3)).reloadData();
                break;
        }
    }

    public ActivityTabBinding getBinding() {
        return binding;
    }

    class BottomTabClick implements BottomNavigationView.OnNavigationItemSelectedListener {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.action_tab1:
                    ((MainTabActivity)context).invalidateMenu();
                    binding.viewpager.setCurrentItem(0);
                    break;
                case R.id.action_tab2:
                    ((MainTabActivity)context).invalidateMenu();
                    binding.viewpager.setCurrentItem(1);
                    break;
                case R.id.action_tab3:
                    ((MainTabActivity)context).invalidateMenu();
                    binding.viewpager.setCurrentItem(2);
                    break;
                case R.id.action_tab4:
                    ((MainTabActivity)context).invalidateMenu();
                    binding.viewpager.setCurrentItem(3);
                    break;
            }
            return true;
        }
    }

}
