package com.station;

import android.app.ActivityManager;
import android.app.ActivityOptions;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import android.support.animation.DynamicAnimation;
import android.support.animation.SpringAnimation;
import android.support.animation.SpringForce;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.station.activity.MainTabActivity;
import com.station.databinding.ActivityEnterBinding;
import com.station.util.PreferencesUtil;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    public Intent bindintent,vpnintent,startintent;
    private boolean flag=true,tag=false;
    public String error="警号不能为空，请重新登陆";
    public String progress="com.zl.policestore.vpn.service.VpnKeepService";
    public ActivityManager activityManger;
    private SpringAnimation animation;
    public List<ActivityManager.RunningServiceInfo> serviceList;
    private ActivityEnterBinding binding;
    public Handler handler=new Handler() {
        public void handleMessage(android.os.Message msg) {
            switch (msg.what) {
                case 2:
                    turn();
                    //text();
                    break;
                case 3:
                    activityManger = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
                    serviceList=activityManger.getRunningServices(Integer.MAX_VALUE);

                    for(ActivityManager.RunningServiceInfo info:serviceList){
                        if(info.service.getClassName().equals(progress)||tag){
                            flag=true;
                            break;
                        }else{
                            flag=false;
                        }
                    }
                    if(!flag){
                        //statictext.setText("初始化失败");
                        //Toast.makeText(MainActivity.this, "移动警务未安装", Toast.LENGTH_SHORT).show();


                        new AlertDialog.Builder(MainActivity.this).setTitle("系统提示")
                                .setMessage("请先安装移动警务平台")
                                .setPositiveButton("确定",new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        finish();
                                    }
                                }).setCancelable(false).show();
                    }
                    break;
            }
        }
    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_enter);
        startintent=new Intent(this,MainTabActivity.class);
        PreferencesUtil.getInstance(this).setLongitude("");
        PreferencesUtil.getInstance(this).setLatitude("");
        SpringForce spring = new SpringForce(0)
                .setDampingRatio(SpringForce.DAMPING_RATIO_LOW_BOUNCY)
                .setStiffness(SpringForce.STIFFNESS_VERY_LOW);
        animation=new SpringAnimation(binding.logoimg, DynamicAnimation.TRANSLATION_Y).setSpring(spring).setStartValue(-700);
        animation.addEndListener(new DynamicAnimation.OnAnimationEndListener() {
            @Override
            public void onAnimationEnd(DynamicAnimation animation, boolean canceled, float value, float velocity) {
                /*getStatus();
                handler.sendEmptyMessageDelayed(3, 3000);*/
                //test();
                turn();
            }
        });
        animation.start();
        /*PrefrementUtil.getInstance(this).setPoliceCode("000006");
        startActivity(startintent);
        finish();*/


    }

    public void test(){
        startActivity(startintent);
        finish();
    }

    public void text(){
        PreferencesUtil.getInstance(this).setLoginSFZH("330282199207221111");
        PreferencesUtil.getInstance(this).setLastLoginPoliceCode("121230");
        int x=(int)binding.logoimg.getTranslationX();
        int y=(int)binding.logoimg.getTranslationY();
        if(android.os.Build.VERSION.SDK_INT>=23){
            startActivity(startintent, ActivityOptions.makeClipRevealAnimation(binding.logoimg,x,y,binding.logoimg.getWidth(),binding.logoimg.getHeight()).toBundle());
        }else{
            startActivity(startintent);
        }
        finish();
    }

    public void turn(){
        Bundle b = getIntent().getExtras();
        if(b!=null){
            String ss = getIntent().getExtras().getString("POLICE_STORE");
            String sfzh=getIntent().getExtras().getString("SFZH");
            if(ss==null||ss.equals("")){
                Toast.makeText(MainActivity.this, error, Toast.LENGTH_LONG).show();
                finish();
            }else{
                Log.e("Mytext","sfzh-->"+sfzh);
                PreferencesUtil.getInstance(this).setLoginSFZH(sfzh);
                PreferencesUtil.getInstance(this).setLastLoginPoliceCode(ss);
                int x=(int)binding.logoimg.getTranslationX();
                int y=(int)binding.logoimg.getTranslationY();
                if(android.os.Build.VERSION.SDK_INT>=23){
                    startActivity(startintent, ActivityOptions.makeClipRevealAnimation(binding.logoimg,x,y,binding.logoimg.getWidth(),binding.logoimg.getHeight()).toBundle());
                }else{
                    startActivity(startintent);
                }
                finish();
            }
        }else{
            Toast.makeText(MainActivity.this, error, Toast.LENGTH_LONG).show();
            finish();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //unbindService(vpnConn);
    }
}
