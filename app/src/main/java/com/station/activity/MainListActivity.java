package com.station.activity;

import com.station.model.MainListModel;

/**
 * Created by nbzl on 2017/10/16.
 */
public class MainListActivity extends BaseActivity {

    private MainListModel model;
    @Override
    protected void after() {
        super.after();
        model=new MainListModel(MainListActivity.this);
        AddContentView(model.getBinding().getRoot());
    }
}
