package com.station.activity;

import android.support.v4.app.FragmentManager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.google.gson.Gson;
import com.station.R;
import com.station.bean.BaseBean;
import com.station.bean.FileEvent;
import com.station.bean.PatrolEvent;
import com.station.bean.Tab2ParamEvent;
import com.station.bean.Tab2RefreshEvent;
import com.station.bean.UpImgBean;
import com.station.http.MNetHttp;
import com.station.listener.HttpListener;
import com.station.listener.LoadStatusListener;
import com.station.listener.UpImageListener;
import com.station.model.DetailModel;
import com.station.util.PreferencesUtil;
import com.station.util.Static;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by nbzl on 2017/10/18.
 */
public class Tab2DetailActivity extends BaseActivity {
    private DetailModel model;
    private FragmentManager manager;
    private String fileIds = "";
    public Map<String, String> params;

    @Override
    protected void after() {
        super.after();
        if(getIntent().getBooleanExtra("isEdit",false)){
            handerBackToolar("编辑");
        }else{
            handerBackToolar("详情");
        }

        params = new HashMap<String, String>();
        manager = getSupportFragmentManager();
        model = new DetailModel(Tab2DetailActivity.this, manager, getIntent().getBooleanExtra("isEdit", false), new LoadStatusImpl());
        //model.setEditStatus(getIntent().getBooleanExtra("isEdit",false));
        AddContentView(model.getBinding().getRoot());

        baseBinding.toolbar.setOnMenuItemClickListener(new MenuItemClick());

    }

    @Subscribe(threadMode = ThreadMode.MAIN, sticky = true)
    public void getEditFragment(PatrolEvent event) {
        params.putAll(event.map);
        //Gson gson = new Gson();
        //String str = gson.toJson(event.map);
        //Log.e("Mytext", "getEditFragment--json-->" + str);
    }

    @Subscribe(threadMode = ThreadMode.MAIN, sticky = true)
    public void gteUpFile(FileEvent event) {

        showProgress("正在上传");
        fileIds = "";
        //Gson gson = new Gson();
        //String str = gson.toJson(event.filelist);
        //Log.e("Mytext", "gteUpFile--json-->" + str);

        if (event.filelist.size() > 0) {
            MNetHttp.getInstance().rxUpBitmap(Static.UPIMG, event.filelist, new HashMap<String, String>(), UpImgBean.class, new UpFileListener());

        }else{
            params.put("fjids", fileIds);
            Tab2ParamEvent eventfile = new Tab2ParamEvent(params);
            EventBus.getDefault().post(eventfile);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN, sticky = true)
    public void submit(Tab2ParamEvent event) {
        Gson gson = new Gson();
        String str = gson.toJson(event.params);
        Log.e("Mytext", "gteUpFile--json-->" + str);
        event.params.put("policecode", PreferencesUtil.getInstance(Tab2DetailActivity.this).getLastLoginPoliceCode());
        MNetHttp.getInstance().rxPostRequest(Static.INPUTTAB2, event.params, BaseBean.class, new SubmitListener());
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if(getIntent().getBooleanExtra("isEdit", false)){
            getMenuInflater().inflate(R.menu.menu_up, menu);
        }

        return true;
    }

    class SubmitListener implements HttpListener {

        @Override
        public void success(Object obj) {

            if (((BaseBean) obj).getReturncode().equals("200")) {
                Static.showToast(Tab2DetailActivity.this, "提交成功");
                //Intent backIntent=new Intent
                Tab2RefreshEvent event = new Tab2RefreshEvent(true);
                EventBus.getDefault().post(event);
                finish();
            } else {
                Static.showToast(Tab2DetailActivity.this, "提交失败," + ((BaseBean) obj).getReturnstr());
            }
            progressDialog.dismiss();
        }

        @Override
        public void fail(String msg) {
            Static.showToast(Tab2DetailActivity.this, msg);
            progressDialog.dismiss();
        }
    }

    class UpFileListener implements UpImageListener {

        @Override
        public void start() {

        }

        @Override
        public void success(Object obj) {
            fileIds += ((UpImgBean) obj).getList().get(0).getId() + ",";
        }

        @Override
        public void failure(String msg) {
            Static.showToast(Tab2DetailActivity.this, msg);
        }

        @Override
        public void complete() {
            Log.e("Mytext","fjids-->"+fileIds);
            params.put("fjids", fileIds.substring(0, fileIds.length() - 1));
            Tab2ParamEvent event = new Tab2ParamEvent(params);
            EventBus.getDefault().post(event);

        }
    }

    class LoadStatusImpl implements LoadStatusListener {

        @Override
        public void show() {
            AddContentView(model.getBinding().getRoot());
        }

        @Override
        public void hide(boolean flag, String msg) {
            loadLayoutModel.setLoadStatus(flag, msg, 0);
            AddContentView(loadLayoutModel.getBinding().getRoot());
        }
    }

    class MenuItemClick implements Toolbar.OnMenuItemClickListener {

        @Override
        public boolean onMenuItemClick(MenuItem item) {

            switch (item.getItemId()) {
                case R.id.action_up:
                    model.upRequest(params);
                    break;
            }
            return true;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        model.onDestroy();
    }
}
