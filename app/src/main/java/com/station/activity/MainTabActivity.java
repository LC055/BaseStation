package com.station.activity;

import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.station.R;
import com.station.bean.Tab2RefreshEvent;
import com.station.model.MainTabModel;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

/**
 * Created by nbzl on 2017/10/17.
 */
public class MainTabActivity extends BaseActivity {

    private MainTabModel model;
    private FragmentManager manager;

    @Override
    protected void after() {
        super.after();
        manager = getSupportFragmentManager();
        model=new MainTabModel(MainTabActivity.this,manager);
        baseBinding.toolbar.setLogo(ContextCompat.getDrawable(MainTabActivity.this,R.mipmap.ic_launcher));
        baseBinding.toolbar.setContentInsetsAbsolute(20,0);
        baseBinding.toolbar.setTitle(R.string.app_name);
        baseBinding.toolbar.setTitleMargin(70,0,10,0);
        baseBinding.toolbar.setOnMenuItemClickListener(new MenuItemClick());

        AddContentView(model.getBinding().getRoot());

        EventBus.getDefault().register(MainTabActivity.this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN, sticky = true)
    public void getTab2Refresh(Tab2RefreshEvent event) {
        Log.e("Mytext","getTab2Refresh");
        if(event.isRefresh){
            model.refreshFragment();
        }
    }


    public void invalidateMenu(){
        invalidateOptionsMenu();
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {

        switch (model.getBinding().viewpager.getCurrentItem()){
            case 0:
                menu.findItem(R.id.action_add).setVisible(true);
                menu.findItem(R.id.action_search).setVisible(true);
                break;
            case 1:
                menu.findItem(R.id.action_add).setVisible(true);
                menu.findItem(R.id.action_search).setVisible(true);
                break;
            case 2:
                menu.findItem(R.id.action_add).setVisible(false);
                menu.findItem(R.id.action_search).setVisible(true);
                break;
            case 3:
                menu.findItem(R.id.action_add).setVisible(true);
                menu.findItem(R.id.action_search).setVisible(true);
                break;
        }
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search, menu);
        return true;
    }

    class MenuItemClick implements Toolbar.OnMenuItemClickListener{

        @Override
        public boolean onMenuItemClick(MenuItem item) {
            switch (item.getItemId()){
                case R.id.action_search:
                    model.menuClick();
                    break;
                case R.id.action_add:
                    model.menuAddClick();
                    break;
            }


            return true;
        }
    }
}
