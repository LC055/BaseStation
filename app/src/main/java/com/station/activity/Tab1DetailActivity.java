package com.station.activity;

import android.app.Activity;

import com.station.listener.LoadStatusListener;
import com.station.model.Tab1DetailModel;

/**
 * Created by nbzl on 2017/10/18.
 */
public class Tab1DetailActivity extends BaseActivity {
    private Tab1DetailModel model;

    @Override
    protected void after() {
        super.after();

        if(getIntent().getBooleanExtra("isEdit",false)){
            handerBackToolar("编辑");
        }else{
            handerBackToolar("详情");
        }
        /*if(getIntent().getBooleanExtra("isEdit", false)){
            handerBackToolar("编辑");
        }else{

        }*/

        model = new Tab1DetailModel(Tab1DetailActivity.this, getSupportFragmentManager(),new LoadStatusImpl());
        //AddContentView(model.getBinding().getRoot());
    }

    class LoadStatusImpl implements LoadStatusListener {

        @Override
        public void show() {
            AddContentView(model.getBinding().getRoot());
        }

        @Override
        public void hide(boolean flag, String msg) {
            loadLayoutModel.setLoadStatus(flag, msg, 0);
            AddContentView(loadLayoutModel.getBinding().getRoot());
        }
    }
}
