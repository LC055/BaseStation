package com.station.fragment;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;

import com.station.activity.Tab2DetailActivity;
import com.station.listener.LoadStatusListener;
import com.station.model.Tab2Model;

/**
 * Created by nbzl on 2017/10/17.
 */
@SuppressLint("ValidFragment")
public class Tab2Fragment extends BaseFragment {

    private Tab2Model model;

    private FragmentManager manager;
    private Handler handler=new Handler(){
        @Override
        public void handleMessage(Message msg) {
            AddBaseContentView(model.getBinding().getRoot());
        }
    };

    public Tab2Fragment(FragmentManager manager){
        this.manager=manager;
    }

    @Override
    protected void initContent(LayoutInflater inflater) {
        super.initContent(inflater);
        model=new Tab2Model(getActivity(),manager,new LoadStatusImpl());
    }

    public void showMenu(){
        model.showMenu();
    }

    public void menuAddClick(){
        Intent addIntent=new Intent(getActivity(),Tab2DetailActivity.class);
        addIntent.putExtra("isEdit",true);
        startActivity(addIntent);
    }

    @Override
    public void loaddata() {
        super.loaddata();
        AddBaseContentView(loadLayoutModel.getBinding().getRoot());
        model.postRequest();
    }

    public void reloadData(){
        AddBaseContentView(loadLayoutModel.getBinding().getRoot());
        model.reloadRequest();
    }

    class LoadStatusImpl implements LoadStatusListener {

        @Override
        public void show() {

            handler.sendMessageDelayed(new Message(),3000);
        }

        @Override
        public void hide(boolean flag, String msg) {
            loadLayoutModel.setLoadStatus(flag, msg, 0);
            AddBaseContentView(loadLayoutModel.getBinding().getRoot());
        }
    }
}
