package com.station.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.station.bean.CheckBean;
import com.station.model.CheckModel;

import java.util.List;

/**
 * Created by nbzl on 2017/10/12.
 */
public class CheckFragment extends Fragment {
    private CheckModel model;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        model=new CheckModel(getActivity(),getArguments().getBoolean("isEdit",false));
        return model.getBinding().getRoot();
    }

    public CheckModel getModel(){
        return model;
    }

    public void setModel(List<CheckBean.ListBeanX> checklist){
        model.setModel(checklist);
    }
}
