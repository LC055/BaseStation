package com.station.fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.station.bean.ZXJBean;
import com.station.model.AddImgModel;

import java.util.List;

/**
 * Created by nbzl on 2017/10/13.
 */
@SuppressLint("ValidFragment")
public class AddImgFragment extends Fragment {
    private AddImgModel model;

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        model=new AddImgModel(getActivity(),getArguments().getBoolean("isEdit",false));
        return model.getBinding().getRoot();

    }

    public AddImgModel getModel(){return model;}

    public void setModel(List<ZXJBean.FJBean> bean){
        model.setModel(bean);
    }
}
