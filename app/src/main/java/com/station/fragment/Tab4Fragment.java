package com.station.fragment;

import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;

import com.station.listener.LoadStatusListener;
import com.station.model.Tab4Model;

/**
 * Created by nbzl on 2017/10/17.
 */
public class Tab4Fragment extends BaseFragment {

    private Tab4Model model;
    private Handler handler=new Handler(){
        @Override
        public void handleMessage(Message msg) {
            AddBaseContentView(model.getBinding().getRoot());
        }
    };

    @Override
    protected void initContent(LayoutInflater inflater) {
        super.initContent(inflater);
        model=new Tab4Model(getActivity(),new LoadStatusImpl());
    }

    public void showMenu(){
        model.showMenu();
    }

    public void menuAddClick(){
        model.menuAddClick();
    }

    @Override
    public void loaddata() {
        super.loaddata();
        AddBaseContentView(loadLayoutModel.getBinding().getRoot());
        model.postRequest();
    }

    public void reloadData(){
        AddBaseContentView(loadLayoutModel.getBinding().getRoot());
        model.reloadRequest();
    }

    class LoadStatusImpl implements LoadStatusListener {

        @Override
        public void show() {

            handler.sendMessageDelayed(new Message(),3000);
        }

        @Override
        public void hide(boolean flag, String msg) {
            loadLayoutModel.setLoadStatus(flag, msg, 0);
            AddBaseContentView(loadLayoutModel.getBinding().getRoot());
        }
    }
}
