package com.station.fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.station.bean.BaseTypeBean;
import com.station.bean.ZXJBean;
import com.station.model.EditModel;

import java.util.List;

/**
 * Created by nbzl on 2017/10/12.
 */
@SuppressLint("ValidFragment")
public class EditFragment extends Fragment {
    private EditModel model;
    private FragmentManager manager;

    public EditFragment(FragmentManager manager){
        super();
        this.manager=manager;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        model=new EditModel(getActivity(),manager,getArguments().getBoolean("isEdit",false));
        return model.getBinder().getRoot();
    }

    public void setModel(ZXJBean zxjBean){
        model.setModel(zxjBean);
    }

    public void setSpinnerData(List<BaseTypeBean> baseList){
        model.setSpinnerData(baseList);
    }

    public EditModel getModel(){
        return model;
    }
}
