package com.station.fragment;

import android.annotation.SuppressLint;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;

import com.station.listener.LoadStatusListener;
import com.station.model.Tab1Model;
import com.station.model.Tab3Model;

/**
 * Created by nbzl on 2017/10/17.
 */
@SuppressLint("ValidFragment")
public class Tab3Fragment extends BaseFragment {

    private Tab3Model model;
    private FragmentManager manager;
    private Handler handler=new Handler(){
        @Override
        public void handleMessage(Message msg) {
            AddBaseContentView(model.getBinding().getRoot());
        }
    };

    public Tab3Fragment(FragmentManager manager){
        this.manager=manager;
    }

    @Override
    protected void initContent(LayoutInflater inflater) {
        super.initContent(inflater);
        model=new Tab3Model(getActivity(),manager,new LoadStatusImpl());
    }

    public void showMenu(){
        model.showMenu();
    }

    @Override
    public void loaddata() {
        super.loaddata();
        AddBaseContentView(loadLayoutModel.getBinding().getRoot());
        model.postRequest();
    }

    class LoadStatusImpl implements LoadStatusListener {

        @Override
        public void show() {

            handler.sendMessageDelayed(new Message(),3000);
        }

        @Override
        public void hide(boolean flag, String msg) {
            loadLayoutModel.setLoadStatus(flag, msg, 0);
            AddBaseContentView(loadLayoutModel.getBinding().getRoot());
        }
    }
}
