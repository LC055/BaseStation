package com.station.fragment;

import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;

import com.station.activity.Tab1DetailActivity;
import com.station.listener.LoadStatusListener;
import com.station.model.Tab1Model;

/**
 * Created by nbzl on 2017/10/17.
 */
public class Tab1Fragment extends BaseFragment {

    private Tab1Model model;

    private Handler handler=new Handler(){
        @Override
        public void handleMessage(Message msg) {
            //loadLayoutModel.setLoadStatus(false,"",0);
            AddBaseContentView(model.getBinding().getRoot());
        }
    };
    @Override
    protected void initContent(LayoutInflater inflater) {
        super.initContent(inflater);
        model=new Tab1Model(getActivity(),new LoadStatusImpl());
    }

    @Override
    public void loaddata() {
        super.loaddata();
        AddBaseContentView(loadLayoutModel.getBinding().getRoot());
        model.postRequest();
    }

    public void reloadData(){
        AddBaseContentView(loadLayoutModel.getBinding().getRoot());
        model.reloadRequest();
    }

    public void showMenu(){
        model.showMenu();
    }

    public void menuAddClick(){
        Intent addIntent=new Intent(getActivity(),Tab1DetailActivity.class);
        addIntent.putExtra("isEdit",true);
        startActivity(addIntent);
    }

    class LoadStatusImpl implements LoadStatusListener {

        @Override
        public void show() {

            handler.sendMessageDelayed(new Message(),3000);
        }

        @Override
        public void hide(boolean flag, String msg) {
            loadLayoutModel.setLoadStatus(flag, msg, 0);
            AddBaseContentView(loadLayoutModel.getBinding().getRoot());
        }
    }
}
