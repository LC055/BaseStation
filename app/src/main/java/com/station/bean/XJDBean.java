package com.station.bean;

/**
 * Created by nbzl on 2017/10/19.
 */
public class XJDBean {

    private String id;
    private String mainid;
    private String checksorid;
    private String isnormal;
    private String issolve;
    private String checkremark;
    private String sortname;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMainid() {
        return mainid;
    }

    public void setMainid(String mainid) {
        this.mainid = mainid;
    }

    public String getChecksorid() {
        return checksorid;
    }

    public void setChecksorid(String checksorid) {
        this.checksorid = checksorid;
    }

    public String getIsnormal() {
        return isnormal;
    }

    public void setIsnormal(String isnormal) {
        this.isnormal = isnormal;
    }

    public String getIssolve() {
        return issolve;
    }

    public void setIssolve(String issolve) {
        this.issolve = issolve;
    }

    public String getCheckremark() {
        return checkremark;
    }

    public void setCheckremark(String checkremark) {
        this.checkremark = checkremark;
    }

    public String getSortname() {
        return sortname;
    }

    public void setSortname(String sortname) {
        this.sortname = sortname;
    }
}
