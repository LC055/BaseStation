package com.station.bean;

import java.util.List;

/**
 * Created by nbzl on 2017/10/19.
 */
public class JZListBean extends BaseBean{
    private List<JZBean> list;

    public List<JZBean> getList() {
        return list;
    }

    public void setList(List<JZBean> list) {
        this.list = list;
    }
}
