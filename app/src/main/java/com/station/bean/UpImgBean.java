package com.station.bean;

import java.util.List;

/**
 * Created by nbzl on 2017/10/25.
 */
public class UpImgBean extends BaseBean{


    /**
     * count : 0
     * list : [{"fjurl":"150891351885653.jpg","fjmc":"0.jpg","id":"6edea1c1-3a7c-45a8-8d14-282e2b0367e3"}]
     */

    private String count;
    private List<ListBean> list;

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public List<ListBean> getList() {
        return list;
    }

    public void setList(List<ListBean> list) {
        this.list = list;
    }

    public static class ListBean {
        /**
         * fjurl : 150891351885653.jpg
         * fjmc : 0.jpg
         * id : 6edea1c1-3a7c-45a8-8d14-282e2b0367e3
         */

        private String fjurl;
        private String fjmc;
        private String id;

        public String getFjurl() {
            return fjurl;
        }

        public void setFjurl(String fjurl) {
            this.fjurl = fjurl;
        }

        public String getFjmc() {
            return fjmc;
        }

        public void setFjmc(String fjmc) {
            this.fjmc = fjmc;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }
    }
}
