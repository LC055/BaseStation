package com.station.bean;

/**
 * Created by nbzl on 2017/10/19.
 */
public class JZBean {
    private String id;
    private String basicname;//基站名称
    private String deptname;//所属单位
    private String addr;//地址
    private String jw_height;//机房高度
    private String tx_height;//天线高度
    private String basictype;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBasicname() {
        return basicname;
    }

    public void setBasicname(String basicname) {
        this.basicname = basicname;
    }

    public String getDeptname() {
        return deptname;
    }

    public void setDeptname(String deptname) {
        this.deptname = deptname;
    }

    public String getAddr() {
        return addr;
    }

    public void setAddr(String addr) {
        this.addr = addr;
    }

    public String getJw_height() {
        return jw_height;
    }

    public void setJw_height(String jw_height) {
        this.jw_height = jw_height;
    }

    public String getTx_height() {
        return tx_height;
    }

    public void setTx_height(String tx_height) {
        this.tx_height = tx_height;
    }

    public String getBasictype() {
        return basictype;
    }

    public void setBasictype(String basictype) {
        this.basictype = basictype;
    }
}
