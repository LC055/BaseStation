package com.station.bean;

import java.util.List;

/**
 * Created by nbzl on 2017/10/23.
 */
public class ZXJBean{

    private String returncode;
    private String returnstr;
    private DataBean data;

    public String getReturncode() {
        return returncode;
    }

    public void setReturncode(String returncode) {
        this.returncode = returncode;
    }

    public String getReturnstr() {
        return returnstr;
    }

    public void setReturnstr(String returnstr) {
        this.returnstr = returnstr;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class FJBean{

        /**
         * id : cbd91921-6756-428a-b4d4-6584844d239e
         * fjmc : Tulips.jpg
         * mainid : 9ee0ef93-cd15-476a-a628-12fac0cae712
         * fjurl : 150880943945064.jpg
         */

        private String id;
        private String fjmc;
        private String mainid;
        private String fjurl;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getFjmc() {
            return fjmc;
        }

        public void setFjmc(String fjmc) {
            this.fjmc = fjmc;
        }

        public String getMainid() {
            return mainid;
        }

        public void setMainid(String mainid) {
            this.mainid = mainid;
        }

        public String getFjurl() {
            return fjurl;
        }

        public void setFjurl(String fjurl) {
            this.fjurl = fjurl;
        }
    }

    public static class DataBean {
        /**
         * id : 400aba51-444d-4221-ab4b-d0d50ab8ea8f
         * basicname : null
         * basicid : null
         * check_date : 2017-10-19 09:40:34
         * check_user : 系统管理员
         * remark : 测试备注
         * deptcode : 330201010000
         * ip : null
         * replay_date : null
         * wendu : 12
         * shidu : 23
         * deptname : 测试单位1下级单位1
         * type : 0
         * queryStart : null
         * queryEnd : null
         * createdate : 2017-10-19 09:41:13
         */

        private String id;
        private String basicname;
        private String basicid;
        private String check_date;
        private String check_user;
        private String remark;
        private String deptcode;
        private String ip;
        private String replay_date;
        private String wendu;
        private String shidu;
        private String deptname;
        private String type;
        private String queryStart;
        private String queryEnd;
        private String createdate;

        private List<FJBean> fjlist;

        public List<FJBean> getFjlist() {
            return fjlist;
        }

        public void setFjlist(List<FJBean> fjlist) {
            this.fjlist = fjlist;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getBasicname() {
            return basicname;
        }

        public void setBasicname(String basicname) {
            this.basicname = basicname;
        }

        public String getBasicid() {
            return basicid;
        }

        public void setBasicid(String basicid) {
            this.basicid = basicid;
        }

        public String getCheck_date() {
            return check_date;
        }

        public void setCheck_date(String check_date) {
            this.check_date = check_date;
        }

        public String getCheck_user() {
            return check_user;
        }

        public void setCheck_user(String check_user) {
            this.check_user = check_user;
        }

        public String getRemark() {
            return remark;
        }

        public void setRemark(String remark) {
            this.remark = remark;
        }

        public String getDeptcode() {
            return deptcode;
        }

        public void setDeptcode(String deptcode) {
            this.deptcode = deptcode;
        }

        public String getIp() {
            return ip;
        }

        public void setIp(String ip) {
            this.ip = ip;
        }

        public String getReplay_date() {
            return replay_date;
        }

        public void setReplay_date(String replay_date) {
            this.replay_date = replay_date;
        }

        public String getWendu() {
            return wendu;
        }

        public void setWendu(String wendu) {
            this.wendu = wendu;
        }

        public String getShidu() {
            return shidu;
        }

        public void setShidu(String shidu) {
            this.shidu = shidu;
        }

        public String getDeptname() {
            return deptname;
        }

        public void setDeptname(String deptname) {
            this.deptname = deptname;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getQueryStart() {
            return queryStart;
        }

        public void setQueryStart(String queryStart) {
            this.queryStart = queryStart;
        }

        public String getQueryEnd() {
            return queryEnd;
        }

        public void setQueryEnd(String queryEnd) {
            this.queryEnd = queryEnd;
        }

        public String getCreatedate() {
            return createdate;
        }

        public void setCreatedate(String createdate) {
            this.createdate = createdate;
        }
    }
}
