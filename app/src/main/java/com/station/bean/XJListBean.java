package com.station.bean;

import java.util.List;

/**
 * Created by nbzl on 2017/10/19.
 */
public class XJListBean extends BaseBean {
    private List<XJBean> list;

    public List<XJBean> getList() {
        return list;
    }

    public void setList(List<XJBean> list) {
        this.list = list;
    }
}
