package com.station.bean;

import java.util.List;
import java.util.Map;

/**
 * Created by nbzl on 2017/10/25.
 */
public class Tab2ParamEvent {
    public Map<String,String> params;

    public Tab2ParamEvent(Map<String,String> params){
        this.params=params;
    }
}
