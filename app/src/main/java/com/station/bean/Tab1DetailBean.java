package com.station.bean;

/**
 * Created by nbzl on 2017/10/20.
 */
public class Tab1DetailBean extends BaseBean {

    private Tab1DataBean data;

    public Tab1DataBean getData() {
        return data;
    }

    public void setData(Tab1DataBean data) {
        this.data = data;
    }
}
