package com.station.bean;

import java.util.List;

/**
 * Created by nbzl on 2017/10/20.
 */
public class Tab1DataBean {


    /**
     * id : 1
     * basicname : 测试1
     * addr : 77
     * longitude : 1231
     * latitude : 23
     * jw_height : 12
     * tx_height : 12
     * basictype : 0
     * rate : 0
     * channelnum : 112
     * linktype : 0
     * linkstripwidth : 1
     * linkid : 1
     * cost : 1
     * supplytype : 0
     * voltage : 123
     * gbtime : 123
     * createdate : 12312
     * subordinatesystem : 123
     * contractor : 12
     * maintenanceunit : 123
     * telephone : 12312412
     * deptcode : 330201010000
     * deptname : 测试单位1下级单位1
     * channeltype : 0
     * channelname : null
     * channelrate : null
     * ratenum : null
     * compName : 中国移动
     * compId : 4
     */

    private String id;
    private String basicname;
    private String addr;
    private String longitude;
    private String latitude;
    private String jw_height;
    private String tx_height;
    private String basictype;
    private String rate;
    private String channelnum;
    private String linktype;
    private String linkstripwidth;
    private String linkid;
    private String cost;
    private String supplytype;
    private String voltage;
    private String gbtime;
    private String createdate;
    private String subordinatesystem;
    private String contractor;
    private String contractor_name;
    private String maintenanceunit;
    private String telephone;
    private String deptcode;
    private String deptname;
    private String channeltype;
    private String channelname;
    private String channelrate;
    private String ratenum;
    private String compName;
    private String compId;
    private List<Tab1DataListBean> list;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBasicname() {
        return basicname;
    }

    public void setBasicname(String basicname) {
        this.basicname = basicname;
    }

    public String getAddr() {
        return addr;
    }

    public void setAddr(String addr) {
        this.addr = addr;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getJw_height() {
        return jw_height;
    }

    public void setJw_height(String jw_height) {
        this.jw_height = jw_height;
    }

    public String getTx_height() {
        return tx_height;
    }

    public void setTx_height(String tx_height) {
        this.tx_height = tx_height;
    }

    public String getBasictype() {
        return basictype;
    }

    public void setBasictype(String basictype) {
        this.basictype = basictype;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getChannelnum() {
        return channelnum;
    }

    public void setChannelnum(String channelnum) {
        this.channelnum = channelnum;
    }

    public String getLinktype() {
        return linktype;
    }

    public void setLinktype(String linktype) {
        this.linktype = linktype;
    }

    public String getLinkstripwidth() {
        return linkstripwidth;
    }

    public void setLinkstripwidth(String linkstripwidth) {
        this.linkstripwidth = linkstripwidth;
    }

    public String getLinkid() {
        return linkid;
    }

    public void setLinkid(String linkid) {
        this.linkid = linkid;
    }

    public String getCost() {
        return cost;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }

    public String getSupplytype() {
        return supplytype;
    }

    public void setSupplytype(String supplytype) {
        this.supplytype = supplytype;
    }

    public String getVoltage() {
        return voltage;
    }

    public void setVoltage(String voltage) {
        this.voltage = voltage;
    }

    public String getGbtime() {
        return gbtime;
    }

    public void setGbtime(String gbtime) {
        this.gbtime = gbtime;
    }

    public String getCreatedate() {
        return createdate;
    }

    public void setCreatedate(String createdate) {
        this.createdate = createdate;
    }

    public String getSubordinatesystem() {
        return subordinatesystem;
    }

    public void setSubordinatesystem(String subordinatesystem) {
        this.subordinatesystem = subordinatesystem;
    }

    public String getContractor() {
        return contractor;
    }

    public void setContractor(String contractor) {
        this.contractor = contractor;
    }

    public String getMaintenanceunit() {
        return maintenanceunit;
    }

    public void setMaintenanceunit(String maintenanceunit) {
        this.maintenanceunit = maintenanceunit;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getDeptcode() {
        return deptcode;
    }

    public void setDeptcode(String deptcode) {
        this.deptcode = deptcode;
    }

    public String getDeptname() {
        return deptname;
    }

    public void setDeptname(String deptname) {
        this.deptname = deptname;
    }

    public String getChanneltype() {
        return channeltype;
    }

    public void setChanneltype(String channeltype) {
        this.channeltype = channeltype;
    }

    public String getChannelname() {
        return channelname;
    }

    public void setChannelname(String channelname) {
        this.channelname = channelname;
    }

    public String getChannelrate() {
        return channelrate;
    }

    public void setChannelrate(String channelrate) {
        this.channelrate = channelrate;
    }

    public String getRatenum() {
        return ratenum;
    }

    public void setRatenum(String ratenum) {
        this.ratenum = ratenum;
    }

    public String getCompName() {
        return compName;
    }

    public void setCompName(String compName) {
        this.compName = compName;
    }

    public String getCompId() {
        return compId;
    }

    public void setCompId(String compId) {
        this.compId = compId;
    }

    public List<Tab1DataListBean> getList() {
        return list;
    }

    public void setList(List<Tab1DataListBean> list) {
        this.list = list;
    }

    public String getContractor_name() {
        return contractor_name;
    }

    public void setContractor_name(String contractor_name) {
        this.contractor_name = contractor_name;
    }
}
