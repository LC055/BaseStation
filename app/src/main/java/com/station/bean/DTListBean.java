package com.station.bean;

import java.util.List;

/**
 * Created by nbzl on 2017/10/20.
 */
public class DTListBean extends BaseBean{

    private List<DTBean> list;

    public List<DTBean> getList() {
        return list;
    }

    public void setList(List<DTBean> list) {
        this.list = list;
    }
}
