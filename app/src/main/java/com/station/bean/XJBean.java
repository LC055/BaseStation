package com.station.bean;

import java.util.List;

/**
 * Created by nbzl on 2017/10/19.
 */
public class XJBean {
    private String id;
    private String basicname;//基站名称
    private String deptname;//检查单位
    private String check_user;//巡检人
    private String check_date;//巡检日期
    private String type;//审核状态 0：否 1：是

    private List<XJDBean> list;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBasicname() {
        return basicname;
    }

    public void setBasicname(String basicname) {
        this.basicname = basicname;
    }

    public String getDeptname() {
        return deptname;
    }

    public void setDeptname(String deptname) {
        this.deptname = deptname;
    }

    public String getCheck_user() {
        return check_user;
    }

    public void setCheck_user(String check_user) {
        this.check_user = check_user;
    }

    public String getCheck_date() {
        return check_date;
    }

    public void setCheck_date(String check_date) {
        this.check_date = check_date;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<XJDBean> getList() {
        return list;
    }

    public void setList(List<XJDBean> list) {
        this.list = list;
    }
}
