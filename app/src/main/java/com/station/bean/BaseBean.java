package com.station.bean;

/**
 * Created by nbzl on 2017/10/19.
 */
public class BaseBean {
    private String returncode;
    private String returnstr;

    public String getReturncode() {
        return returncode;
    }

    public void setReturncode(String returncode) {
        this.returncode = returncode;
    }

    public String getReturnstr() {
        return returnstr;
    }

    public void setReturnstr(String returnstr) {
        this.returnstr = returnstr;
    }
}
