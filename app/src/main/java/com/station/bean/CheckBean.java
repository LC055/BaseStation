package com.station.bean;

import java.util.List;

/**
 * Created by nbzl on 2017/10/23.
 */
public class CheckBean extends BaseBean {

    /**
     * size : 5
     * list : [{"id":"1","sortName":"防雷接地","pid":"0","sort":"1","csize":0,"isnormal":null,"issolve":null,"checkremark":null,"sortname":null,"list":[{"id":"2","sortName":"设备直流工作接地、保护接地连接无松动","pid":"1","sort":"1","csize":0,"isnormal":"0","issolve":"1","checkremark":"OK","sortname":null,"list":null},{"id":"3","sortName":"并紧固接电线、接地汇集线和接地引入线","pid":"1","sort":"2","csize":0,"isnormal":"1","issolve":null,"checkremark":null,"sortname":null,"list":null}]},{"id":"4","sortName":"用电线路","pid":"0","sort":"2","csize":0,"isnormal":null,"issolve":null,"checkremark":null,"sortname":null,"list":[{"id":"5","sortName":"交流供电线路无松动，交流电源是否正常","pid":"4","sort":"1","csize":0,"isnormal":"0","issolve":"0","checkremark":"解决不了","sortname":null,"list":null},{"id":"6","sortName":"设备接地是否规范，阻抗是否符合要求","pid":"4","sort":"2","csize":0,"isnormal":"1","issolve":null,"checkremark":null,"sortname":null,"list":null}]},{"id":"7","sortName":"UPS系统","pid":"0","sort":"3","csize":0,"isnormal":null,"issolve":null,"checkremark":null,"sortname":null,"list":[{"id":"b6ae8ce2-f499-448d-8b9c-46bb11e7aeab","sortName":"切断交流输入，检查UPS转电池逆变","pid":"7","sort":"1","csize":0,"isnormal":"0","issolve":"0","checkremark":"me too","sortname":null,"list":null}]}]
     */

    private int size;
    private List<ListBeanX> list;

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public List<ListBeanX> getList() {
        return list;
    }

    public void setList(List<ListBeanX> list) {
        this.list = list;
    }

    public static class ListBeanX {
        /**
         * id : 1
         * sortName : 防雷接地
         * pid : 0
         * sort : 1
         * csize : 0
         * isnormal : null
         * issolve : null
         * checkremark : null
         * sortname : null
         * list : [{"id":"2","sortName":"设备直流工作接地、保护接地连接无松动","pid":"1","sort":"1","csize":0,"isnormal":"0","issolve":"1","checkremark":"OK","sortname":null,"list":null},{"id":"3","sortName":"并紧固接电线、接地汇集线和接地引入线","pid":"1","sort":"2","csize":0,"isnormal":"1","issolve":null,"checkremark":null,"sortname":null,"list":null}]
         */

        private String id;
        private String sortName;
        private String pid;
        private String sort;
        private String csize;
        private Object isnormal;
        private Object issolve;
        private Object checkremark;
        private Object sortname;
        private List<ListBean> list;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getSortName() {
            return sortName;
        }

        public void setSortName(String sortName) {
            this.sortName = sortName;
        }

        public String getPid() {
            return pid;
        }

        public void setPid(String pid) {
            this.pid = pid;
        }

        public String getSort() {
            return sort;
        }

        public void setSort(String sort) {
            this.sort = sort;
        }

        public String getCsize() {
            return csize;
        }

        public void setCsize(String csize) {
            this.csize = csize;
        }

        public Object getIsnormal() {
            return isnormal;
        }

        public void setIsnormal(Object isnormal) {
            this.isnormal = isnormal;
        }

        public Object getIssolve() {
            return issolve;
        }

        public void setIssolve(Object issolve) {
            this.issolve = issolve;
        }

        public Object getCheckremark() {
            return checkremark;
        }

        public void setCheckremark(Object checkremark) {
            this.checkremark = checkremark;
        }

        public Object getSortname() {
            return sortname;
        }

        public void setSortname(Object sortname) {
            this.sortname = sortname;
        }

        public List<ListBean> getList() {
            return list;
        }

        public void setList(List<ListBean> list) {
            this.list = list;
        }

        public static class ListBean {
            /**
             * id : 2
             * sortName : 设备直流工作接地、保护接地连接无松动
             * pid : 1
             * sort : 1
             * csize : 0
             * isnormal : 0
             * issolve : 1
             * checkremark : OK
             * sortname : null
             * list : null
             */

            private String id;
            private String sortName;
            private String pid;
            private String sort;
            private String csize;
            private String isnormal;
            private String issolve;
            private String checkremark;
            private Object sortname;
            private Object list;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getSortName() {
                return sortName;
            }

            public void setSortName(String sortName) {
                this.sortName = sortName;
            }

            public String getPid() {
                return pid;
            }

            public void setPid(String pid) {
                this.pid = pid;
            }

            public String getSort() {
                return sort;
            }

            public void setSort(String sort) {
                this.sort = sort;
            }

            public String getCsize() {
                return csize;
            }

            public void setCsize(String csize) {
                this.csize = csize;
            }

            public String getIsnormal() {
                return isnormal;
            }

            public void setIsnormal(String isnormal) {
                this.isnormal = isnormal;
            }

            public String getIssolve() {
                return issolve;
            }

            public void setIssolve(String issolve) {
                this.issolve = issolve;
            }

            public String getCheckremark() {
                return checkremark;
            }

            public void setCheckremark(String checkremark) {
                this.checkremark = checkremark;
            }

            public Object getSortname() {
                return sortname;
            }

            public void setSortname(Object sortname) {
                this.sortname = sortname;
            }

            public Object getList() {
                return list;
            }

            public void setList(Object list) {
                this.list = list;
            }
        }
    }
}
