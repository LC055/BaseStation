package com.station.bean;

/**
 * Created by nbzl on 2017/10/25.
 */
public class YYSBean {
    public String code;
    public String text;

    public YYSBean(){}
    public YYSBean(String code,String text){
        this.code=code;
        this.text=text;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
