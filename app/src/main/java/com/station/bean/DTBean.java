package com.station.bean;

/**
 * Created by nbzl on 2017/10/20.
 */
public class DTBean {

    /**
     * id : 91d8e367-15bc-4a25-acd7-5b48f26a4f3f
     * brand_id : 1241
     * device_id : 21GDSF256
     * deptcode : 330202000000
     * fzr : 李白
     * type : 2
     * call_number : 126123
     * fzr_telepone : 1562312167
     * borrow_deptcode : null
     * borrow_user : null
     * brandname : 海能达PD780G
     * operator : null
     * borrow_time : null
     * cancel_time : null
     * createdate : 2017-10-17 09:50:56
     * deptname : 测试单位2
     * remark : 呵呵哒\(^o^)/~
     * operator_telephone : null
     * giveback_time : null
     * borrow_type : null
     * borrow_remark : null
     */

    private String id;
    private String brand_id;
    private String device_id;
    private String deptcode;
    private String fzr;
    private String type;
    private String call_number;
    private String fzr_telepone;
    private String borrow_deptcode;
    private String borrow_user;
    private String brandname;
    private String createdate;
    private String deptname;
    private String remark;
    private String operator_telephone;
    private String giveback_time;
    private String borrow_type;
    private String borrow_remark;
    private String pdtesn;
    private String gdzcxh;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBrand_id() {
        return brand_id;
    }

    public void setBrand_id(String brand_id) {
        this.brand_id = brand_id;
    }

    public String getDevice_id() {
        return device_id;
    }

    public void setDevice_id(String device_id) {
        this.device_id = device_id;
    }

    public String getDeptcode() {
        return deptcode;
    }

    public void setDeptcode(String deptcode) {
        this.deptcode = deptcode;
    }

    public String getFzr() {
        return fzr;
    }

    public void setFzr(String fzr) {
        this.fzr = fzr;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCall_number() {
        return call_number;
    }

    public void setCall_number(String call_number) {
        this.call_number = call_number;
    }

    public String getFzr_telepone() {
        return fzr_telepone;
    }

    public void setFzr_telepone(String fzr_telepone) {
        this.fzr_telepone = fzr_telepone;
    }

    public String getBorrow_deptcode() {
        return borrow_deptcode;
    }

    public void setBorrow_deptcode(String borrow_deptcode) {
        this.borrow_deptcode = borrow_deptcode;
    }

    public String getBorrow_user() {
        return borrow_user;
    }

    public void setBorrow_user(String borrow_user) {
        this.borrow_user = borrow_user;
    }

    public String getBrandname() {
        return brandname;
    }

    public void setBrandname(String brandname) {
        this.brandname = brandname;
    }

    public String getCreatedate() {
        return createdate;
    }

    public void setCreatedate(String createdate) {
        this.createdate = createdate;
    }

    public String getDeptname() {
        return deptname;
    }

    public void setDeptname(String deptname) {
        this.deptname = deptname;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getOperator_telephone() {
        return operator_telephone;
    }

    public void setOperator_telephone(String operator_telephone) {
        this.operator_telephone = operator_telephone;
    }

    public String getGiveback_time() {
        return giveback_time;
    }

    public void setGiveback_time(String giveback_time) {
        this.giveback_time = giveback_time;
    }

    public String getBorrow_type() {
        return borrow_type;
    }

    public void setBorrow_type(String borrow_type) {
        this.borrow_type = borrow_type;
    }

    public String getBorrow_remark() {
        return borrow_remark;
    }

    public void setBorrow_remark(String borrow_remark) {
        this.borrow_remark = borrow_remark;
    }

    public String getPdtesn() {
        return pdtesn;
    }

    public void setPdtesn(String pdtesn) {
        this.pdtesn = pdtesn;
    }

    public String getGdzcxh() {
        return gdzcxh;
    }

    public void setGdzcxh(String gdzcxh) {
        this.gdzcxh = gdzcxh;
    }
}
