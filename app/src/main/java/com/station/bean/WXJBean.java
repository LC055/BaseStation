package com.station.bean;

/**
 * Created by nbzl on 2017/10/20.
 */
public class WXJBean {

    private String id;
    private String deptcode;
    private String basicname;
    private String check_start_time;
    private String check_end_time;
    private String rate;
    private String now_time;
    private String ischecked;
    private String check_date;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDeptcode() {
        return deptcode;
    }

    public void setDeptcode(String deptcode) {
        this.deptcode = deptcode;
    }

    public String getBasicname() {
        return basicname;
    }

    public void setBasicname(String basicname) {
        this.basicname = basicname;
    }

    public String getCheck_start_time() {
        return check_start_time;
    }

    public void setCheck_start_time(String check_start_time) {
        this.check_start_time = check_start_time;
    }

    public String getCheck_end_time() {
        return check_end_time;
    }

    public void setCheck_end_time(String check_end_time) {
        this.check_end_time = check_end_time;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public Object getNow_time() {
        return now_time;
    }

    public void setNow_time(String now_time) {
        this.now_time = now_time;
    }

    public String getIschecked() {
        return ischecked;
    }

    public void setIschecked(String ischecked) {
        this.ischecked = ischecked;
    }

    public String getCheck_date() {
        return check_date;
    }

    public void setCheck_date(String check_date) {
        this.check_date = check_date;
    }
}
