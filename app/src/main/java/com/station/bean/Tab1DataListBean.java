package com.station.bean;

/**
 * Created by nbzl on 2017/10/20.
 */
public class Tab1DataListBean {


    /**
     * id : a4d76ce2-5bc5-4501-a59e-c994104cc6a8
     * channelname : 1235125
     * channelrate : 123
     * ratenum : 123
     * mainid : 1
     * channelsort : null
     */

    private String id;
    private String channelname;
    private String channelrate;
    private String ratenum;
    private String mainid;
    private String channelsort;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getChannelname() {
        return channelname;
    }

    public void setChannelname(String channelname) {
        this.channelname = channelname;
    }

    public String getChannelrate() {
        return channelrate;
    }

    public void setChannelrate(String channelrate) {
        this.channelrate = channelrate;
    }

    public String getRatenum() {
        return ratenum;
    }

    public void setRatenum(String ratenum) {
        this.ratenum = ratenum;
    }

    public String getMainid() {
        return mainid;
    }

    public void setMainid(String mainid) {
        this.mainid = mainid;
    }

    public String getChannelsort() {
        return channelsort;
    }

    public void setChannelsort(String channelsort) {
        this.channelsort = channelsort;
    }
}
