package com.station.bean;

import java.util.List;

/**
 * Created by nbzl on 2017/10/20.
 */
public class WXJListBean extends BaseBean{


    private List<WXJBean> list;

    public List<WXJBean> getList() {
        return list;
    }

    public void setList(List<WXJBean> list) {
        this.list = list;
    }
}
