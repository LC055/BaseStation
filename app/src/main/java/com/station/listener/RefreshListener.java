package com.station.listener;

/**
 * Created by nbzl on 2017/8/15.
 */
public interface RefreshListener {
    public void refresh();
    public void loadmore();
}
