package com.station.listener;

/**
 * Created by nbzl on 2017/8/31.
 */
public interface LoadStatusListener {

    public void show();
    public void hide(boolean flag, String msg);
}
