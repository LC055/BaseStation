package com.station.listener;

/**
 * Created by nbzl on 2016/11/21.
 */
public interface RecyclerItemClick {

    void onItemClick(int position);
    void onItemLongClick(int position);
    void onMoreClick(int position);
}
