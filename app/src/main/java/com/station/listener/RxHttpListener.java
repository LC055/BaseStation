package com.station.listener;

/**
 * Created by nbzl on 2017/10/23.
 */
public interface RxHttpListener {
    void finish();
    void fail(String msg);
    void next(Object obj);
}
