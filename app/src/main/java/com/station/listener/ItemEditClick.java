package com.station.listener;

/**
 * Created by nbzl on 2017/10/17.
 */
public interface ItemEditClick {
    public void delete(int position);
    public void edit(int position);
    public void click(int position);
}
