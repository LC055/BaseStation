package com.station.listener;

import java.util.Map;

/**
 * Created by nbzl on 2017/10/25.
 */
public interface FragmentGetMap {
    public void getBackMap(Map<String,String> map);
}
