package com.station.listener;

/**
 * Created by nbzl on 2017/9/7.
 */
public interface UpImageListener {
    public void start();
    public void success(Object obj);
    public void failure(String msg);
    public void complete();
}
