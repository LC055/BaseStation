package com.station.listener;

/**
 * Created by nbzl on 2017/8/21.
 */
public interface GralleryImgListener {

    public void success(String filepath);
    public void fail(String msg);
}
