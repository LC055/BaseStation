package com.station.widget;

import android.content.Context;
import android.support.animation.SpringAnimation;
import android.util.AttributeSet;
import android.widget.ImageView;

/**
 * Created by nbzl on 2017/10/13. true:0 false:90
 */
public class RotateImgView extends ImageView {
    private boolean tag=true;
    public RotateImgView(Context context) {
        this(context,null);
    }

    public RotateImgView(Context context, AttributeSet attrs) {
        super(context, attrs);
        tag=true;
    }

    public void toZero(){
        this.setPivotX(getWidth()/2);
        this.setPivotY(getHeight()/2);
        this.setRotation(90);
        tag=false;
    }
    public void toRotate(){
        this.setPivotX(getWidth()/2);
        this.setPivotY(getHeight()/2);
        this.setRotation(0);
        tag=true;
    }

    public void toggle(){
        if(tag){
            toZero();
        }else{
            toRotate();
        }
    }

    public void toggle(SpringAnimation animationshow,SpringAnimation animationhide){
        if(tag){
            animationhide.start();
            toZero();
        }else{
            animationshow.start();
            toRotate();
        }
    }

}
