package com.station.widget;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by nbzl on 2017/7/28.
 */
public class RVItemDecoration extends RecyclerView.ItemDecoration {

    private int decoration;//边距大小 px

    public RVItemDecoration(int decoration) {
        this.decoration = decoration;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        super.getItemOffsets(outRect, view, parent, state);
        int lastPosition = state.getItemCount() - 1;
        RecyclerView.LayoutManager layoutManager = parent.getLayoutManager();
        int current = parent.getChildLayoutPosition(view);
        if (current == -1) return;
        if (current == lastPosition) {//判断是否为最后一个item
            outRect.set(0, 0, 0, 0);
        } else {
            outRect.set(0, 0, 0, decoration);
        }
    }
}
