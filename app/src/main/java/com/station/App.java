package com.station;

import android.content.Context;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by nbzl on 2017/10/19.
 */
public class App extends MultiDexApplication {
    private Map<String,List<String>> baseTypeMap=new HashMap<String,List<String>>();
    private Map<String,String> basetypeItem=new HashMap<String,String>();

    private Map<String,List<String>> xdtypeMap=new HashMap<String,List<String>>();
    private Map<String,String> xdtypeItem=new HashMap<String,String>();;

    private Map<String,List<String>> datetypeMap=new HashMap<String,List<String>>();
    private Map<String,String> datetypeItem=new HashMap<String,String>();;

    private Map<String,List<String>> jzxdtypeMap=new HashMap<String,List<String>>();
    private Map<String,String> jzxdtypeItem=new HashMap<String,String>();

    private Map<String,List<String>> llutypeMap=new HashMap<String,List<String>>();
    private Map<String,String> llutypeItem=new HashMap<String,String>();

    private Map<String,List<String>> gdtypeMap=new HashMap<String,List<String>>();
    private Map<String,String> gdtypeItem=new HashMap<String,String>();

    @Override
    public void onCreate() {
        super.onCreate();
        initStringArray(baseTypeMap,basetypeItem,R.array.basetype);
        initStringArray(xdtypeMap,xdtypeItem,R.array.basetype);
        initStringArray(datetypeMap,datetypeItem,R.array.jzrate);
        initStringArray(jzxdtypeMap,jzxdtypeItem,R.array.xdtype);
        initStringArray(llutypeMap,llutypeItem,R.array.llutype);
        initStringArray(gdtypeMap,gdtypeItem,R.array.gdtype);
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }


    public void initStringArray(Map<String,List<String>> typemap,Map<String,String> typeItem,int stringarr){
        /*typemap=new HashMap<String,List<String>>();
        typeItem=new HashMap<String,String>();*/
        String[] type=getResources().getStringArray(stringarr);
        List<String> key= new ArrayList<String>();
        List<String> value= new ArrayList<String>();
        for(String event:type){
            if(Integer.valueOf(event.substring(0,event.indexOf("|")))==-1){
                key.add("");
            }else{
                key.add(event.substring(0,event.indexOf("|")));
            }
            value.add(event.substring(event.indexOf("|")+1,event.length()));
            typeItem.put(event.substring(0,event.indexOf("|")),event.substring(event.indexOf("|")+1,event.length()));
        }
        typemap.put("key",key);
        typemap.put("value",value);
    }

    public Map<String,List<String>> getBaseTypeMap(){
        return baseTypeMap;
    }

    public Map<String, String> getBasetypeItem() {
        return basetypeItem;
    }

    public Map<String, List<String>> getXdtypeMap() {
        return xdtypeMap;
    }

    public Map<String, String> getXdtypeItem() {
        return xdtypeItem;
    }

    public Map<String, String> getDatetypeItem() {
        return datetypeItem;
    }

    public Map<String, List<String>> getDatetypeMap() {
        return datetypeMap;
    }

    public Map<String, String> getJzxdtypeItem() {
        return jzxdtypeItem;
    }


    public Map<String, List<String>> getJzxdtypeMap() {
        return jzxdtypeMap;
    }

    public Map<String, String> getLlutypeItem() {
        return llutypeItem;
    }

    public Map<String, List<String>> getLlutypeMap() {
        return llutypeMap;
    }

    public Map<String, String> getGdtypeItem() {
        return gdtypeItem;
    }

    public Map<String, List<String>> getGdtypeMap() {
        return gdtypeMap;
    }

}
