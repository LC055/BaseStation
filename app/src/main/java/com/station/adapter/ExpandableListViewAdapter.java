package com.station.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import com.station.R;
import com.station.bean.CheckBean;
import com.station.util.BindingUtil;

import org.w3c.dom.Text;

import java.util.List;

/**
 * Created by nbzl on 2017/10/23.
 */
public class ExpandableListViewAdapter extends BaseExpandableListAdapter {
    private List<CheckBean.ListBeanX> data;
    private Context context;

    public ExpandableListViewAdapter(Context context,List<CheckBean.ListBeanX> data){
        this.data=data;
        this.context=context;
    }
    @Override
    public int getGroupCount() {
        return data.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return data.get(groupPosition).getList().size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return data.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return data.get(groupPosition).getList().get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {

        GroupHolder groupHolder = null;
        if (convertView == null){
            convertView = LayoutInflater.from(context).inflate(R.layout.item_expandable_group, null);
            groupHolder=new GroupHolder();
            groupHolder.title=(TextView)convertView.findViewById(R.id.title);
            convertView.setTag(groupHolder);
        }else{
            groupHolder = (GroupHolder)convertView.getTag();
        }
        groupHolder.title.setText(data.get(groupPosition).getSortName());
        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        ItemHolder itemHolder = null;
        if (convertView == null){
            convertView = LayoutInflater.from(context).inflate(R.layout.item_expandable_item, null);
            itemHolder=new ItemHolder();
            itemHolder.title=(TextView)convertView.findViewById(R.id.title);
            itemHolder.normal=(TextView)convertView.findViewById(R.id.normal);
            itemHolder.solve=(TextView)convertView.findViewById(R.id.solve);
            itemHolder.remark=(TextView)convertView.findViewById(R.id.remark);
            convertView.setTag(itemHolder);
        }else{
            itemHolder = (ItemHolder)convertView.getTag();
        }
        itemHolder.title.setText(BindingUtil.isEmpty(data.get(groupPosition).getList().get(childPosition).getSortName()));
        itemHolder.normal.setText(BindingUtil.isNormal("状态：",data.get(groupPosition).getList().get(childPosition).getIsnormal()));
        itemHolder.solve.setText(BindingUtil.isSolve("是否解决：",data.get(groupPosition).getList().get(childPosition).getIssolve()));
        itemHolder.remark.setText(BindingUtil.isEmpty("备注：",data.get(groupPosition).getList().get(childPosition).getCheckremark()));
        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    class GroupHolder{
        TextView title;
    }
    class ItemHolder{
        TextView title;
        TextView normal;
        TextView solve;
        TextView remark;
    }
}
