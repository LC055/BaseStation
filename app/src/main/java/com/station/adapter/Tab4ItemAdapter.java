package com.station.adapter;

import android.content.Context;
import android.util.Log;
import android.view.View;

import com.daimajia.swipe.SwipeLayout;
import com.station.R;
import com.station.bean.DTBean;
import com.station.databinding.ItemTab4Binding;
import com.station.listener.ItemEditClick;
import com.station.listener.RecyclerItemClick;
import com.station.util.BindingUtil;

import java.util.List;

/**
 * Created by nbzl on 2017/10/17.
 */
public class Tab4ItemAdapter extends BaseSwipeAdapter {
    private ItemEditClick editClick;

    public Tab4ItemAdapter(Context context,List<Object> datas, int layoutId, RecyclerItemClick listener, ItemEditClick editClick) {
        this(context,datas, layoutId, 0, listener,editClick);
    }

    public Tab4ItemAdapter(Context context,List<Object> datas, int layoutId, int mVariableId, RecyclerItemClick listener, ItemEditClick editClick) {
        super(context,datas, layoutId, mVariableId, listener);
        this.editClick=editClick;
    }

    @Override
    public void BinderHolder(final BaseHolder holder, final int position) {
        if (holder instanceof ContentHolder) {
            if(holder.getBinding() instanceof ItemTab4Binding){
                mItemManger.bindView(((ItemTab4Binding)(holder.getBinding())).getRoot(),position);
                Log.e("Mytext","ItemTab1Binding-->"+position);
                ((ItemTab4Binding)holder.getBinding()).swipelayout.setShowMode(SwipeLayout.ShowMode.LayDown);
                ((ItemTab4Binding)holder.getBinding()).setPosition((position+1)+"");
                ((ItemTab4Binding)holder.getBinding()).setDevicename(context.getString(R.string.label_device)+""+BindingUtil.isEmpty(((DTBean) datas.get(position)).getBrandname()));
                ((ItemTab4Binding)holder.getBinding()).setDeviceid(context.getString(R.string.label_deviceid)+""+BindingUtil.isEmpty(((DTBean) datas.get(position)).getDevice_id()));
                ((ItemTab4Binding)holder.getBinding()).setFzr(context.getString(R.string.label_fzr)+""+BindingUtil.isEmpty(((DTBean) datas.get(position)).getFzr()));
                ((ItemTab4Binding)holder.getBinding()).setDevicestatue(BindingUtil.isDtType(context.getString(R.string.label_device_state),((DTBean) datas.get(position)).getType()));
                ((ItemTab4Binding)holder.getBinding()).setPdtesn(BindingUtil.isEmpty(context.getString(R.string.label_pds),((DTBean) datas.get(position)).getPdtesn()));
                ((ItemTab4Binding)holder.getBinding()).setGdzc(BindingUtil.isEmpty(context.getString(R.string.label_gdzc),((DTBean) datas.get(position)).getGdzcxh()));
                ((ItemTab4Binding)holder.getBinding()).operation.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //editClick.click(position);
                        ((ItemTab4Binding)holder.getBinding()).swipelayout.toggle();
                        /*if(mItemManger.isOpen(position)){
                            mItemManger.closeItem(position);
                        }else{
                            mItemManger.openItem(position);
                        }*/
                    }
                });
                ((ItemTab4Binding)holder.getBinding()).content.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        editClick.click(position);
                        mItemManger.closeItem(position);
                    }
                });
                ((ItemTab4Binding)holder.getBinding()).editbtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        editClick.edit(position);
                        mItemManger.closeItem(position);
                    }
                });
                ((ItemTab4Binding)holder.getBinding()).delbtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        editClick.delete(position);
                        mItemManger.closeItem(position);
                    }
                });
            }
        }
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipelayout;
    }
}
