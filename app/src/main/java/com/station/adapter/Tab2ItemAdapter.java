package com.station.adapter;

import android.content.Context;
import android.util.Log;
import android.view.View;

import com.daimajia.swipe.SwipeLayout;
import com.station.R;
import com.station.bean.XJBean;
import com.station.databinding.ItemTab2Binding;
import com.station.databinding.ItemTab4Binding;
import com.station.listener.ItemEditClick;
import com.station.listener.RecyclerItemClick;

import java.util.List;

/**
 * Created by nbzl on 2017/10/17.
 */
public class Tab2ItemAdapter extends BaseSwipeAdapter {
    private ItemEditClick editClick;

    public Tab2ItemAdapter(Context context,List<Object> datas, int layoutId, RecyclerItemClick listener, ItemEditClick editClick) {
        this(context,datas, layoutId, 0, listener,editClick);
    }

    public Tab2ItemAdapter(Context context,List<Object> datas, int layoutId, int mVariableId, RecyclerItemClick listener, ItemEditClick editClick) {
        super(context,datas, layoutId, mVariableId, listener);
        this.editClick=editClick;
    }

    @Override
    public void BinderHolder(final BaseHolder holder, final int position) {
        if (holder instanceof ContentHolder) {
            if(holder.getBinding() instanceof ItemTab2Binding){
                mItemManger.bindView(((ItemTab2Binding)(holder.getBinding())).getRoot(),position);
                Log.e("Mytext","ItemTab1Binding-->"+position);
                ((ItemTab2Binding)holder.getBinding()).swipelayout.setShowMode(SwipeLayout.ShowMode.LayDown);
                ((ItemTab2Binding)holder.getBinding()).setPosition((position+1)+"");
                ((ItemTab2Binding)holder.getBinding()).setItem(((XJBean)datas.get(position)));

                ((ItemTab2Binding)holder.getBinding()).operation.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //editClick.click(position);
                        ((ItemTab2Binding)holder.getBinding()).swipelayout.toggle();
                        /*if(mItemManger.isOpen(position)){
                            mItemManger.closeItem(position);
                        }else{
                            mItemManger.openItem(position);
                        }*/
                    }
                });
                ((ItemTab2Binding)holder.getBinding()).content.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        editClick.click(position);
                        mItemManger.closeItem(position);
                    }
                });
                ((ItemTab2Binding)holder.getBinding()).editbtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        editClick.edit(position);
                        mItemManger.closeItem(position);
                    }
                });
                ((ItemTab2Binding)holder.getBinding()).delbtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        editClick.delete(position);
                        mItemManger.closeItem(position);
                    }
                });
            }
        }
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipelayout;
    }
}
