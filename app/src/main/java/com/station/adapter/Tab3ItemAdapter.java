package com.station.adapter;

import com.station.bean.WXJBean;
import com.station.databinding.ItemTab3Binding;
import com.station.listener.RecyclerItemClick;

import java.util.List;

/**
 * Created by nbzl on 2017/10/13.
 */
public class Tab3ItemAdapter extends BaseRcAdapter {

    public Tab3ItemAdapter(List<Object> datas, int layoutId, int mVariableId, RecyclerItemClick listener) {
        super(datas, layoutId, mVariableId, listener);
    }

    @Override
    public void BinderHolder(BaseHolder holder, int position) {

        if(holder instanceof ContentHolder){
            if(holder.getBinding() instanceof ItemTab3Binding){
                ((ItemTab3Binding)holder.getBinding()).setPosition((position+1)+"");
                ((ItemTab3Binding)holder.getBinding()).setItem((WXJBean)datas.get(position));
            }
        }

    }
}
