package com.station.adapter;

import android.content.Context;
import android.view.View;

import com.daimajia.swipe.SwipeLayout;
import com.station.R;
import com.station.bean.JZBean;
import com.station.databinding.ItemTab1Binding;
import com.station.databinding.ItemTab4Binding;
import com.station.listener.ItemEditClick;
import com.station.listener.RecyclerItemClick;

import java.util.List;

/**
 * Created by nbzl on 2017/10/17.
 */
public class Tab1ItemAdapter extends BaseSwipeAdapter {
    private ItemEditClick editClick;

    public Tab1ItemAdapter(Context context,List<Object> datas, int layoutId, RecyclerItemClick listener,ItemEditClick editClick) {
        this(context,datas, layoutId, 0, listener,editClick);
    }

    public Tab1ItemAdapter(Context context,List<Object> datas, int layoutId, int mVariableId, RecyclerItemClick listener, ItemEditClick editClick) {
        super(context,datas, layoutId, mVariableId, listener);
        this.editClick=editClick;
    }

    @Override
    public void BinderHolder(final BaseHolder holder, final int position) {
        if (holder instanceof ContentHolder) {
            if(holder.getBinding() instanceof ItemTab1Binding){
                //datas.get(position)
                ((ItemTab1Binding)holder.getBinding()).setBasetypename(context.getString(R.string.label_lx)+app.getBasetypeItem().get(((JZBean)(datas.get(position))).getBasictype()));
                mItemManger.bindView(((ItemTab1Binding)(holder.getBinding())).getRoot(),position);
                ((ItemTab1Binding)holder.getBinding()).swipelayout.setShowMode(SwipeLayout.ShowMode.LayDown);
                ((ItemTab1Binding)holder.getBinding()).setPosition((position+1)+"");
                ((ItemTab1Binding)holder.getBinding()).setItem((JZBean) datas.get(position));


                ((ItemTab1Binding)holder.getBinding()).operation.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //editClick.click(position);
                        ((ItemTab1Binding)holder.getBinding()).swipelayout.toggle();
                        /*if(mItemManger.isOpen(position)){
                            mItemManger.closeItem(position);
                        }else{
                            mItemManger.openItem(position);
                        }*/
                    }
                });
                ((ItemTab1Binding)holder.getBinding()).content.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        editClick.click(position);
                        mItemManger.closeItem(position);
                    }
                });
                ((ItemTab1Binding)holder.getBinding()).editbtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        editClick.edit(position);
                        mItemManger.closeItem(position);
                    }
                });
                ((ItemTab1Binding)holder.getBinding()).delbtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        editClick.delete(position);
                        mItemManger.closeItem(position);
                    }
                });
            }
        }
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipelayout;
    }
}
