package com.station.adapter;

import com.station.listener.RecyclerItemClick;

import java.util.List;

/**
 * Created by nbzl on 2017/10/13.
 */
public class MainItemAdapter extends BaseRcAdapter {

    public MainItemAdapter(List<Object> datas, int layoutId, int mVariableId, RecyclerItemClick listener) {
        super(datas, layoutId, mVariableId, listener);
    }

    @Override
    public void BinderHolder(BaseHolder holder, int position) {

        if(holder instanceof ContentHolder){
            holder.getBinding().setVariable(mVariableId, (datas.get(position)));
        }

    }
}
