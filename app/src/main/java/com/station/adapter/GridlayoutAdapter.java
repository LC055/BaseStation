package com.station.adapter;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.support.v4.content.ContextCompat;

import com.station.R;
import com.station.databinding.GridImgBinding;
import com.station.listener.RecyclerItemClick;

import java.util.List;
import java.util.Map;

/**
 * Created by nbzl on 2017/10/24.
 */
public class GridlayoutAdapter extends BaseRcAdapter {
    private Context context;

    public GridlayoutAdapter(Context context,List<Object> datas, int layoutId, int mVariableId, RecyclerItemClick listener) {
        this(datas, layoutId, mVariableId, listener);
        this.context=context;
    }

    public GridlayoutAdapter(List<Object> datas, int layoutId, int mVariableId, RecyclerItemClick listener) {
        super(datas, layoutId, mVariableId, listener);
    }

    @Override
    public void BinderHolder(BaseHolder holder, int position) {
        if (holder instanceof ContentHolder) {
            String path = ((Map<String, Object>) datas.get(position)).get("path").toString();
            Object item = ((Map<String, Object>) datas.get(position)).get("data");
            if (holder.getBinding() instanceof GridImgBinding) {
                if(path.equals("")){
                    ((GridImgBinding) holder.getBinding()).imgview.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.failimg));
                }else{
                    ((GridImgBinding) holder.getBinding()).imgview.setImageBitmap(BitmapFactory.decodeFile(path));
                }

            }

        }
    }
}
