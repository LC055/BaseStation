package com.station.adapter;

import android.app.Activity;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.daimajia.swipe.adapters.RecyclerSwipeAdapter;
import com.station.App;
import com.station.listener.RecyclerItemClick;

import java.util.List;

/**
 * Created by nbzl on 2017/7/26.
 */
public abstract class BaseSwipeAdapter extends RecyclerSwipeAdapter<BaseSwipeAdapter.BaseHolder> {

    protected List<Object> datas;
    protected int layoutId,footlayoutid;
    protected int mVariableId;
    protected RecyclerItemClick listener;
    private int footViewSize = 0;
    private static final int TYPE_HEADER = 0, TYPE_ITEM = 1, TYPE_FOOT = 2;
    protected Context context;
    public App app;

    public BaseSwipeAdapter(Context context,List<Object> datas, int layoutId, int mVariableId, RecyclerItemClick listener) {
        this.datas = datas;
        this.layoutId = layoutId;
        this.mVariableId = mVariableId;
        this.listener=listener;
        this.context=context;
        app=((App)((Activity)context).getApplication());
    }

    @Override
    public BaseHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        BaseHolder holder=null;
        if(viewType==TYPE_FOOT){
            ViewDataBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), footlayoutid, parent, false);
            holder =new FootHolder(binding.getRoot());
            holder.setBinding(binding);
        }else{
            ViewDataBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), layoutId, parent, false);
            holder = new ContentHolder(binding.getRoot());
            holder.setBinding(binding);

        }
        return holder;
    }

    @Override
    public int getItemViewType(int position) {
        int type = TYPE_ITEM;
        if (footViewSize == 1 && position == getItemCount() - 1){
            type = TYPE_FOOT;
        }
        return type;
    }

    @Override
    public void onBindViewHolder(BaseHolder holder, int position) {
        BinderHolder(holder, position);
    }
    public void addFootView(int layoutId) {
        footlayoutid = layoutId;
        footViewSize = 1;
    }
    public abstract void BinderHolder(BaseHolder holder, int position);

    @Override
    public int getItemCount() {
        return datas.size()+footViewSize;
    }


    public class BaseHolder extends RecyclerView.ViewHolder{
        public ViewDataBinding binding;
        public BaseHolder(View itemView) {
            super(itemView);
        }
        public ViewDataBinding getBinding() {
            return binding;
        }

        public void setBinding(ViewDataBinding binding) {
            this.binding = binding;
        }
    }


    public class ContentHolder extends BaseHolder implements View.OnClickListener,View.OnLongClickListener{

        public ContentHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if(listener!=null){
                listener.onItemClick(getPosition());
            }

        }

        @Override
        public boolean onLongClick(View v) {
            if(listener!=null){
                listener.onItemLongClick(getPosition());
            }
            return false;
        }
    }

    public class FootHolder extends BaseHolder{
        public FootHolder(View itemView){
            super(itemView);
        }
    }
}
